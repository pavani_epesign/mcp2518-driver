#include "spi_0.h"
#include "bsp.h"

#include "can_api.h"
#include "can_defines.h"
#include "can_registers.h"
uint32_t data u32_test;
//SI_SEGMENT_VARIABLE(crc16_table,uint16_t,SI_SEG_XDATA);
// *****************************************************************************
// *****************************************************************************
// Section: Defines
// Control Register Reset Values up to FIFOs
//const uint32_t canControlResetValues[20] = {
//   /* Address 0x000 to 0x00C */
//    0x04980760, 0x003E0F0F, 0x000E0303, 0x00021000,
//    /* Address 0x010 to 0x01C */
//    0x00000000, 0x00000000, 0x40400040, 0x00000000,
//   /* Address 0x020 to 0x02C */
//    0x00000000, 0x00000000, 0x00000000, 0x00000000,
//   /* Address 0x030 to 0x03C */
//   0x00000000, 0x00200000, 0x00000000, 0x00000000,
//   /* Address 0x040 to 0x04C */
//   0x00000400, 0x00000000, 0x00000000, 0x00000000
//};
// FIFO Register Reset Values
const uint32_t canFifoResetValues[3] =
  { 0x00600400, 0x00000000, 0x00000000 };

// Filter and Mask Object Reset Values
const uint32_t canFilterObjectResetValues[2] =
  { 0x00000000, 0x00000000 };
//const uint16_t crc16_table[256] =
//  { 0x0000, 0x8005, 0x800F, 0x000A, 0x801B, 0x001E, 0x0014, 0x8011, 0x8033,
//      0x0036, 0x003C, 0x8039, 0x0028, 0x802D, 0x8027, 0x0022, 0x8063, 0x0066,
//      0x006C, 0x8069, 0x0078, 0x807D, 0x8077, 0x0072, 0x0050, 0x8055, 0x805F,
//      0x005A, 0x804B, 0x004E, 0x0044, 0x8041, 0x80C3, 0x00C6, 0x00CC, 0x80C9,
//      0x00D8, 0x80DD, 0x80D7, 0x00D2, 0x00F0, 0x80F5, 0x80FF, 0x00FA, 0x80EB,
//      0x00EE, 0x00E4, 0x80E1, 0x00A0, 0x80A5, 0x80AF, 0x00AA, 0x80BB, 0x00BE,
//      0x00B4, 0x80B1, 0x8093, 0x0096, 0x009C, 0x8099, 0x0088, 0x808D, 0x8087,
//      0x0082, 0x8183, 0x0186, 0x018C, 0x8189, 0x0198, 0x819D, 0x8197, 0x0192,
//      0x01B0, 0x81B5, 0x81BF, 0x01BA, 0x81AB, 0x01AE, 0x01A4, 0x81A1, 0x01E0,
//      0x81E5, 0x81EF, 0x01EA, 0x81FB, 0x01FE, 0x01F4, 0x81F1, 0x81D3, 0x01D6,
//      0x01DC, 0x81D9, 0x01C8, 0x81CD, 0x81C7, 0x01C2, 0x0140, 0x8145, 0x814F,
//      0x014A, 0x815B, 0x015E, 0x0154, 0x8151, 0x8173, 0x0176, 0x017C, 0x8179,
//      0x0168, 0x816D, 0x8167, 0x0162, 0x8123, 0x0126, 0x012C, 0x8129, 0x0138,
//      0x813D, 0x8137, 0x0132, 0x0110, 0x8115, 0x811F, 0x011A, 0x810B, 0x010E,
//      0x0104, 0x8101, 0x8303, 0x0306, 0x030C, 0x8309, 0x0318, 0x831D, 0x8317,
//      0x0312, 0x0330, 0x8335, 0x833F, 0x033A, 0x832B, 0x032E, 0x0324, 0x8321,
//      0x0360, 0x8365, 0x836F, 0x036A, 0x837B, 0x037E, 0x0374, 0x8371, 0x8353,
//      0x0356, 0x035C, 0x8359, 0x0348, 0x834D, 0x8347, 0x0342, 0x03C0, 0x83C5,
//      0x83CF, 0x03CA, 0x83DB, 0x03DE, 0x03D4, 0x83D1, 0x83F3, 0x03F6, 0x03FC,
//      0x83F9, 0x03E8, 0x83ED, 0x83E7, 0x03E2, 0x83A3, 0x03A6, 0x03AC, 0x83A9,
//      0x03B8, 0x83BD, 0x83B7, 0x03B2, 0x0390, 0x8395, 0x839F, 0x039A, 0x838B,
//      0x038E, 0x0384, 0x8381, 0x0280, 0x8285, 0x828F, 0x028A, 0x829B, 0x029E,
//      0x0294, 0x8291, 0x82B3, 0x02B6, 0x02BC, 0x82B9, 0x02A8, 0x82AD, 0x82A7,
//      0x02A2, 0x82E3, 0x02E6, 0x02EC, 0x82E9, 0x02F8, 0x82FD, 0x82F7, 0x02F2,
//      0x02D0, 0x82D5, 0x82DF, 0x02DA, 0x82CB, 0x02CE, 0x02C4, 0x82C1, 0x8243,
//      0x0246, 0x024C, 0x8249, 0x0258, 0x825D, 0x8257, 0x0252, 0x0270, 0x8275,
//      0x827F, 0x027A, 0x826B, 0x026E, 0x0264, 0x8261, 0x0220, 0x8225, 0x822F,
//      0x022A, 0x823B, 0x023E, 0x0234, 0x8231, 0x8213, 0x0216, 0x021C, 0x8219,
//      0x0208, 0x820D, 0x8207, 0x0202 };

#define CRCBASE    0xFFFF
#define CRCUPPER   1
#define SPI_DEFAULT_BUFFER_LENGTH 32
SI_SBIT(CS_PIN, SFR_P1, 1);
// *****************************************************************************
// *****************************************************************************
// Section: Variables

SI_SEGMENT_VARIABLE(spiTransmitBuffer[SPI_DEFAULT_BUFFER_LENGTH], uint8_t,
                    EFM8PDL_SPI0_TX_SEGTYPE);
SI_SEGMENT_VARIABLE(spiReceiveBuffer[SPI_DEFAULT_BUFFER_LENGTH], uint8_t,
                    EFM8PDL_SPI0_RX_SEGTYPE);

int8_t
DRV_SPI_TransferData (uint8_t spiSlaveDeviceIndex, uint8_t *SpiTxData,
                      uint8_t *SpiRxData, uint16_t spiTransferSize)
{
  uint8_t retval = 0;
  spiSlaveDeviceIndex = 0;
  CS_PIN = 0;
  //return spi_master_transfer(SpiTxData, SpiRxData, spiTransferSize);
  retval = SPI0_pollTransfer (SpiTxData, SpiRxData, SPI0_TRANSFER_RXTX,
                              spiTransferSize);
  CS_PIN = 1;
  return retval;
}

// *****************************************************************************
// *****************************************************************************
// Section: Reset

int8_t
DRV_CANFDSPI_Reset (CANFDSPI_MODULE_ID index)
{
  uint16_t spiTransferSize = 2;
  int8_t spiTransferError = 0;

  // Compose command
  spiTransmitBuffer[0] = (uint8_t) (cINSTRUCTION_RESET << 4);
  spiTransmitBuffer[1] = 0;

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);

  return spiTransferError;
}

// *****************************************************************************
// *****************************************************************************
// Section: SPI Access Functions

int8_t
DRV_CANFDSPI_ReadByte (CANFDSPI_MODULE_ID index, uint16_t address, uint8_t *rxd)
{
  uint16_t spiTransferSize = 3;
  int8_t spiTransferError = 0;

  // Compose command
  spiTransmitBuffer[0] = (uint8_t) ((cINSTRUCTION_READ << 4)
      + ((address >> 8) & 0xF));
  spiTransmitBuffer[1] = (uint8_t) (address & 0xFF);
  spiTransmitBuffer[2] = 0;

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);

  // Update data
  *rxd = spiReceiveBuffer[2];

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_WriteByte (CANFDSPI_MODULE_ID index, uint16_t address, uint8_t txd)
{
  uint16_t spiTransferSize = 3;
  int8_t spiTransferError = 0;

  // Compose command
  spiTransmitBuffer[0] = (uint8_t) ((cINSTRUCTION_WRITE << 4)
      + ((address >> 8) & 0xF));
  spiTransmitBuffer[1] = (uint8_t) (address & 0xFF);
  spiTransmitBuffer[2] = txd;

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ReadWord (CANFDSPI_MODULE_ID index, uint16_t address,
                       uint32_t *rxd)
{
  uint8_t i;
  uint32_t x;
  uint16_t spiTransferSize = 6;
  int8_t spiTransferError = 0;

  // Compose command
  spiTransmitBuffer[0] = (uint8_t) ((cINSTRUCTION_READ << 4)
      + ((address >> 8) & 0xF));
  spiTransmitBuffer[1] = (uint8_t) (address & 0xFF);

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);
  if (spiTransferError)
    {
      return spiTransferError;
    }

  // Update data
  *rxd = 0;
  for (i = 2; i < 6; i++)
    {
      x = (uint32_t) spiReceiveBuffer[i];
      *rxd += x << ((i - 2) * 8);
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_WriteWord (CANFDSPI_MODULE_ID index, uint16_t address,
                        uint32_t txd)
{
  uint8_t i;
  uint16_t spiTransferSize = 6;
  int8_t spiTransferError = 0;

  // Compose command
  spiTransmitBuffer[0] = (uint8_t) ((cINSTRUCTION_WRITE << 4)
      + ((address >> 8) & 0xF));
  spiTransmitBuffer[1] = (uint8_t) (address & 0xFF);

  // Split word into 4 bytes and add them to buffer
  for (i = 0; i < 4; i++)
    {
      spiTransmitBuffer[i + 2] = (uint8_t) ((txd >> (i * 8)) & 0xFF);
    }

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ReadHalfWord (CANFDSPI_MODULE_ID index, uint16_t address,
                           uint16_t *rxd)
{
  uint8_t i;
  uint32_t x;
  uint16_t spiTransferSize = 4;
  int8_t spiTransferError = 0;

  // Compose command
  spiTransmitBuffer[0] = (uint8_t) ((cINSTRUCTION_READ << 4)
      + ((address >> 8) & 0xF));
  spiTransmitBuffer[1] = (uint8_t) (address & 0xFF);

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);
  if (spiTransferError)
    {
      return spiTransferError;
    }

  // Update data
  *rxd = 0;
  for (i = 2; i < 4; i++)
    {
      x = (uint32_t) spiReceiveBuffer[i];
      *rxd += x << ((i - 2) * 8);
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_WriteHalfWord (CANFDSPI_MODULE_ID index, uint16_t address,
                            uint16_t txd)
{
  uint8_t i;
  uint16_t spiTransferSize = 4;
  int8_t spiTransferError = 0;

  // Compose command
  spiTransmitBuffer[0] = (uint8_t) ((cINSTRUCTION_WRITE << 4)
      + ((address >> 8) & 0xF));
  spiTransmitBuffer[1] = (uint8_t) (address & 0xFF);

  // Split word into 2 bytes and add them to buffer
  for (i = 0; i < 2; i++)
    {
      spiTransmitBuffer[i + 2] = (uint8_t) ((txd >> (i * 8)) & 0xFF);
    }

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ReadByteArray (CANFDSPI_MODULE_ID index, uint16_t address,
                            uint8_t *rxd, uint16_t nBytes)
{
  uint16_t i;
  uint16_t spiTransferSize = nBytes + 2;
  int8_t spiTransferError = 0;

  // Validate that length of array is sufficient to hold requested number of bytes
  if (spiTransferSize > sizeof(spiTransmitBuffer))
    {
      return -1;
    }

  // Compose command
  spiTransmitBuffer[0] = (uint8_t) ((cINSTRUCTION_READ << 4)
      + ((address >> 8) & 0xF));
  spiTransmitBuffer[1] = (uint8_t) (address & 0xFF);

  // Clear data
  for (i = 2; i < spiTransferSize; i++)
    {
      spiTransmitBuffer[i] = 0;
    }

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);

  // Update data
  for (i = 0; i < nBytes; i++)
    {
      rxd[i] = spiReceiveBuffer[i + 2];
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_WriteByteArray (CANFDSPI_MODULE_ID index, uint16_t address,
                             uint8_t *txd, uint16_t nBytes)
{
  uint16_t i;
  uint16_t spiTransferSize = nBytes + 2;
  int8_t spiTransferError = 0;

  // Validate that length of array is sufficient to hold requested number of bytes
  if (spiTransferSize > sizeof(spiTransmitBuffer))
    {
      return -1;
    }

  // Compose command
  spiTransmitBuffer[0] = (uint8_t) ((cINSTRUCTION_WRITE << 4)
      + ((address >> 8) & 0xF));
  spiTransmitBuffer[1] = (uint8_t) (address & 0xFF);

  // Add data
  for (i = 0; i < nBytes; i++)
    {
      spiTransmitBuffer[i + 2] = txd[i];
    }

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ReadWordArray (CANFDSPI_MODULE_ID index, uint16_t address,
                            uint32_t *rxd, uint16_t nWords)
{
  uint16_t i, j, n;
  REG_t w;
  uint16_t spiTransferSize = nWords * 4 + 2;
  int8_t spiTransferError = 0;

  // Validate that length of array is sufficient to hold requested number of bytes
  if (spiTransferSize > sizeof(spiTransmitBuffer))
    {
      return -1;
    }

  // Compose command
  spiTransmitBuffer[0] = (cINSTRUCTION_READ << 4) + ((address >> 8) & 0xF);
  spiTransmitBuffer[1] = address & 0xFF;

  // Clear data
  for (i = 2; i < spiTransferSize; i++)
    {
      spiTransmitBuffer[i] = 0;
    }

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);
  if (spiTransferError)
    {
      return spiTransferError;
    }

  // Convert Byte array to Word array
  n = 2;
  for (i = 0; i < nWords; i++)
    {
      w.word = 0;
      for (j = 0; j < 4; j++, n++)
        {
          w.byte[j] = spiReceiveBuffer[n];
        }
      rxd[i] = w.word;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_WriteWordArray (CANFDSPI_MODULE_ID index, uint16_t address,
                             uint32_t *txd, uint16_t nWords)
{
  uint16_t i, j, n;
  REG_t w;
  uint16_t spiTransferSize = nWords * 4 + 2;
  int8_t spiTransferError = 0;

  // Validate that length of array is sufficient to hold requested number of bytes
  if (spiTransferSize > sizeof(spiTransmitBuffer))
    {
      return -1;
    }

  // Compose command
  spiTransmitBuffer[0] = (cINSTRUCTION_WRITE << 4) + ((address >> 8) & 0xF);
  spiTransmitBuffer[1] = address & 0xFF;

  // Convert ByteArray to word array
  n = 2;
  for (i = 0; i < nWords; i++)
    {
      w.word = txd[i];
      for (j = 0; j < 4; j++, n++)
        {
          spiTransmitBuffer[n] = w.byte[j];
        }
    }

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_WriteByteSafe (CANFDSPI_MODULE_ID index, uint16_t address,
                            uint8_t txd)
{
  uint16_t crcResult = 0;
  uint16_t spiTransferSize = 5;
  int8_t spiTransferError = 0;

  // Compose command
  spiTransmitBuffer[0] = (uint8_t) ((cINSTRUCTION_WRITE_SAFE << 4)
      + ((address >> 8) & 0xF));
  spiTransmitBuffer[1] = (uint8_t) (address & 0xFF);
  spiTransmitBuffer[2] = txd;

  // Add CRC
  crcResult = DRV_CANFDSPI_CalculateCRC16 (spiTransmitBuffer, 3);
  spiTransmitBuffer[3] = (crcResult >> 8) & 0xFF;
  spiTransmitBuffer[4] = crcResult & 0xFF;

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_WriteWordSafe (CANFDSPI_MODULE_ID index, uint16_t address,
                            uint32_t txd)
{
  uint8_t i;
  uint16_t crcResult = 0;
  uint16_t spiTransferSize = 8;
  int8_t spiTransferError = 0;

  // Compose command
  spiTransmitBuffer[0] = (uint8_t) ((cINSTRUCTION_WRITE_SAFE << 4)
      + ((address >> 8) & 0xF));
  spiTransmitBuffer[1] = (uint8_t) (address & 0xFF);

  // Split word into 4 bytes and add them to buffer
  for (i = 0; i < 4; i++)
    {
      spiTransmitBuffer[i + 2] = (uint8_t) ((txd >> (i * 8)) & 0xFF);
    }

  // Add CRC
  crcResult = DRV_CANFDSPI_CalculateCRC16 (spiTransmitBuffer, 6);
  spiTransmitBuffer[6] = (crcResult >> 8) & 0xFF;
  spiTransmitBuffer[7] = crcResult & 0xFF;

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ReadByteArrayWithCRC (CANFDSPI_MODULE_ID index, uint16_t address,
                                   uint8_t *rxd, uint16_t nBytes, bool fromRam,
                                   uint8_t *crcIsCorrect)
{
  uint8_t i;
  uint16_t crcFromSpiSlave = 0;
  uint16_t crcAtController = 0;
  uint16_t spiTransferSize = nBytes + 5; //first two bytes for sending command & address, third for size, last two bytes for CRC
  int8_t spiTransferError = 0;

  // Validate that length of array is sufficient to hold requested number of bytes
  if (spiTransferSize > sizeof(spiTransmitBuffer))
    {
      return -1;
    }

  // Compose command
  spiTransmitBuffer[0] = (uint8_t) ((cINSTRUCTION_READ_CRC << 4)
      + ((address >> 8) & 0xF));
  spiTransmitBuffer[1] = (uint8_t) (address & 0xFF);
  if (fromRam)
    {
      spiTransmitBuffer[2] = nBytes >> 2;
    }
  else
    {
      spiTransmitBuffer[2] = nBytes;
    }

  // Clear data
  for (i = 3; i < spiTransferSize; i++)
    {
      spiTransmitBuffer[i] = 0;
    }

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);
  if (spiTransferError)
    {
      return spiTransferError;
    }

  // Get CRC from controller
  crcFromSpiSlave = (uint16_t) (spiReceiveBuffer[spiTransferSize - 2] << 8)
      + (uint16_t) (spiReceiveBuffer[spiTransferSize - 1]);

  // Use the receive buffer to calculate CRC
  // First three bytes need to be command
  spiReceiveBuffer[0] = spiTransmitBuffer[0];
  spiReceiveBuffer[1] = spiTransmitBuffer[1];
  spiReceiveBuffer[2] = spiTransmitBuffer[2];
  crcAtController = DRV_CANFDSPI_CalculateCRC16 (spiReceiveBuffer, nBytes + 3);

  // Compare CRC readings
  if (crcFromSpiSlave == crcAtController)
    {
      *crcIsCorrect = true;
    }
  else
    {
      *crcIsCorrect = false;
    }

  // Update data
  for (i = 0; i < nBytes; i++)
    {
      rxd[i] = spiReceiveBuffer[i + 3];
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_WriteByteArrayWithCRC (CANFDSPI_MODULE_ID index, uint16_t address,
                                    uint8_t *txd, uint16_t nBytes, bool fromRam)
{
  uint16_t i;
  uint16_t crcResult = 0;
  uint16_t spiTransferSize = nBytes + 5;
  int8_t spiTransferError = 0;

  // Validate that length of array is sufficient to hold requested number of bytes
  if (spiTransferSize > sizeof(spiTransmitBuffer))
    {
      return -1;
    }
  // Compose command
  spiTransmitBuffer[0] = (uint8_t) ((cINSTRUCTION_WRITE_CRC << 4)
      + ((address >> 8) & 0xF));
  spiTransmitBuffer[1] = (uint8_t) (address & 0xFF);
  if (fromRam)
    {
      spiTransmitBuffer[2] = nBytes >> 2;
    }
  else
    {
      spiTransmitBuffer[2] = nBytes;
    }

  // Add data
  for (i = 0; i < nBytes; i++)
    {
      spiTransmitBuffer[i + 3] = txd[i];
    }

  // Add CRC
  crcResult = DRV_CANFDSPI_CalculateCRC16 (spiTransmitBuffer,
                                           spiTransferSize - 2);
  spiTransmitBuffer[spiTransferSize - 2] = (uint8_t) ((crcResult >> 8) & 0xFF);
  spiTransmitBuffer[spiTransferSize - 1] = (uint8_t) (crcResult & 0xFF);

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_EccEnable (CANFDSPI_MODULE_ID index)
{
  int8_t spiTransferError = 0;
  uint8_t d = 0;

  // Read
  spiTransferError = DRV_CANFDSPI_ReadByte (index, cREGADDR_ECCCON, &d);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  d |= 0x01;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, cREGADDR_ECCCON, d);
  if (spiTransferError)
    {
      return -2;
    }

  return 0;
}

int8_t
DRV_CANFDSPI_RamInit (CANFDSPI_MODULE_ID index, uint8_t d)
{
  uint8_t txd[SPI_DEFAULT_BUFFER_LENGTH / 2];
  uint32_t k;
  uint16_t a;
  int8_t spiTransferError = 0;

  // Prepare data
  for (k = 0; k < SPI_DEFAULT_BUFFER_LENGTH / 2; k++)
    {
      txd[k] = d;
    }

  a = cRAMADDR_START;

  for (k = 0; k < ((cRAM_SIZE / SPI_DEFAULT_BUFFER_LENGTH) * 2); k++)
    {
      spiTransferError = DRV_CANFDSPI_WriteByteArray (
          index, a, txd, SPI_DEFAULT_BUFFER_LENGTH / 2);
      if (spiTransferError)
        {
          return -1;
        }
      a += SPI_DEFAULT_BUFFER_LENGTH / 2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_Configure (CANFDSPI_MODULE_ID index, CAN_CONFIG *config)
{
  REG_CiCON cicon;
  int8_t spiTransferError = 0;
  uint32_t wd;
  uint32_t wd1, wd2;

  //wd = 0x04980760;                      // Reset Value
  cicon.word = 0x0760;                            // Reset Value
  cicon.bF1.DNetFilterCount = config->bF1.DNetFilterCount;
  cicon.bF1.IsoCrcEnable = config->bF1.IsoCrcEnable;
  ;
  cicon.bF1.ProtocolExceptionEventDisable =
      config->bF1.ProtocolExceptionEventDisable;
  cicon.bF1.WakeUpFilterEnable = config->bF1.WakeUpFilterEnable;
  cicon.bF1.WakeUpFilterTime = config->bF1.WakeUpFilterTime;
  cicon.bF1.BitRateSwitchDisable = config->bF1.BitRateSwitchDisable;
  wd1 = cicon.word;
  cicon.word = 0x0498;                          // Reset Value
  cicon.bF2.RestrictReTxAttempts = config->bF2.RestrictReTxAttempts;
  cicon.bF2.EsiInGatewayMode = config->bF2.EsiInGatewayMode;
  cicon.bF2.SystemErrorToListenOnly = config->bF2.SystemErrorToListenOnly;
  cicon.bF2.StoreInTEF = config->bF2.StoreInTEF;
  cicon.bF2.TXQEnable = config->bF2.TXQEnable;
  cicon.bF2.AbortAllTx = config->bF2.AbortAllTx;
  cicon.bF2.TxBandWidthSharing = config->bF2.TxBandWidthSharing;
  wd2 = cicon.word;

  wd = wd1 + (wd2 << 16);
  //ciCon.word = 0x04900760;
  spiTransferError = DRV_CANFDSPI_WriteWord (index, cREGADDR_CiCON, wd);
  //DRV_CANFDSPI_ReadWord(index,cREGADDR_CiCON,&u32_test);
  if (spiTransferError)
    {
      return -1;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ConfigureObjectReset (CAN_CONFIG *config)
{
  REG_CiCON ciCon;
  //ciCon.word = canControlResetValues[cREGADDR_CiCON / 4];
  ciCon.word = 0x0760;
  config->bF1.DNetFilterCount = ciCon.bF1.DNetFilterCount;
  config->bF1.IsoCrcEnable = ciCon.bF1.IsoCrcEnable;
  config->bF1.ProtocolExceptionEventDisable =
      ciCon.bF1.ProtocolExceptionEventDisable;
  config->bF1.WakeUpFilterEnable = ciCon.bF1.WakeUpFilterEnable;
  config->bF1.WakeUpFilterTime = ciCon.bF1.WakeUpFilterTime;
  config->bF1.BitRateSwitchDisable = ciCon.bF1.BitRateSwitchDisable;
  ciCon.word = 0x0498;
  config->bF2.RestrictReTxAttempts = ciCon.bF2.RestrictReTxAttempts;
  config->bF2.EsiInGatewayMode = ciCon.bF2.EsiInGatewayMode;
  config->bF2.SystemErrorToListenOnly = ciCon.bF2.SystemErrorToListenOnly;
  config->bF2.StoreInTEF = ciCon.bF2.StoreInTEF;
  config->bF2.TXQEnable = ciCon.bF2.TXQEnable;
  config->bF2.TxBandWidthSharing = ciCon.bF2.TxBandWidthSharing;

  return 0;
}

int8_t
DRV_CANFDSPI_ReceiveChannelConfigure (CANFDSPI_MODULE_ID index,
                                      CAN_FIFO_CHANNEL channel,
                                      CAN_RX_FIFO_CONFIG *config)
{
  REG_CiFIFOCON ciFifoCon;
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  uint32_t wd;
  uint32_t wd1, wd2;

  if (channel == CAN_TXQUEUE_CH0)
    {
      return -100;
    }

  // Setup FIFO
  ciFifoCon.word = canFifoResetValues[0];

  ciFifoCon.rxBF1.TxEnable = 0;
  ciFifoCon.rxBF1.RxTimeStampEnable = config->RxTimeStampEnable;
  wd1 = ciFifoCon.word;

  ciFifoCon.word = (canFifoResetValues[0] >> 16) & 0xFFFF;
  ciFifoCon.rxBF2.FifoSize = config->FifoSize;
  ciFifoCon.rxBF2.PayLoadSize = config->PayLoadSize;
  wd2 = ciFifoCon.word;

  wd = wd1 + (wd2 << 16);
  //ciFifoCon.word = 0xef600400;

  a = cREGADDR_CiFIFOCON + (channel * CiFIFO_OFFSET);

  spiTransferError = DRV_CANFDSPI_WriteWord (index, a, wd);
  //DRV_CANFDSPI_ReadWord (0,a, &u32_test);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_NominalBitTimeConfigure (CANFDSPI_MODULE_ID index,
                                      CAN_BITTIME_SETUP bitTime,
                                      CAN_SYSCLK_SPEED clk)
{
  int8_t spiTransferError = 0;

  // Decode clk
  switch (clk)
    {
    case CAN_SYSCLK_40M:
      spiTransferError = DRV_CANFDSPI_BitTimeConfigureNominal40MHz (index,
                                                                    bitTime);
      //if (spiTransferError) return spiTransferError;

      //spiTransferError = DRV_CANFDSPI_BitTimeConfigureData40MHz(index, bitTime, sspMode);
      break;
    case CAN_SYSCLK_20M:
      spiTransferError = DRV_CANFDSPI_BitTimeConfigureNominal20MHz (index,
                                                                    bitTime);
      //if (spiTransferError) return spiTransferError;

      //spiTransferError = DRV_CANFDSPI_BitTimeConfigureData20MHz(index, bitTime, sspMode);
      break;
    case CAN_SYSCLK_10M:
      spiTransferError = DRV_CANFDSPI_BitTimeConfigureNominal10MHz (index,
                                                                    bitTime);
      //if (spiTransferError) return spiTransferError;

      //spiTransferError = DRV_CANFDSPI_BitTimeConfigureData10MHz(index, bitTime, sspMode);
      break;
    default:
      spiTransferError = -1;
      break;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_DataBitTimeConfigure (CANFDSPI_MODULE_ID index,
                                   CAN_BITTIME_SETUP bitTime,
                                   CAN_SYSCLK_SPEED clk)
{
  int8_t spiTransferError = 0;

  // Decode clk
  switch (clk)
    {
    case CAN_SYSCLK_40M:

      //spiTransferError = DRV_CANFDSPI_BitTimeConfigureNominal40MHz(index, bitTime);
      spiTransferError = DRV_CANFDSPI_BitTimeConfigureData40MHz (index,
                                                                 bitTime);
      //if (spiTransferError) return spiTransferError;
      break;
    case CAN_SYSCLK_20M:
      //spiTransferError = DRV_CANFDSPI_BitTimeConfigureNominal20MHz(index, bitTime);

      spiTransferError = DRV_CANFDSPI_BitTimeConfigureData20MHz (index,
                                                                 bitTime);
      //if (spiTransferError) return spiTransferError;
      break;
    case CAN_SYSCLK_10M:
      //spiTransferError = DRV_CANFDSPI_BitTimeConfigureNominal10MHz(index, bitTime);

      spiTransferError = DRV_CANFDSPI_BitTimeConfigureData10MHz (index,
                                                                 bitTime);
      //if (spiTransferError) return spiTransferError;
      break;
    default:
      spiTransferError = -1;
      break;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_BitTimeConfigureNominal40MHz (CANFDSPI_MODULE_ID index,
                                           CAN_BITTIME_SETUP bitTime)
{
  int8_t spiTransferError = 0;
  uint32_t wd, wd1, wd2;
  REG_CiNBTCFG ciNbtcfg;

  //ciNbtcfg.word = canControlResetValues[cREGADDR_CiNBTCFG / 4];

  // Arbitration Bit rate
  switch (bitTime)
    {
    // All 500K
    case CAN_500K_1M:
    case CAN_500K_2M:
    case CAN_500K_3M:
    case CAN_500K_4M:
    case CAN_500K_5M:
    case CAN_500K_6M7:
    case CAN_500K_8M:
    case CAN_500K_10M:
      ciNbtcfg.word = 0x003E;
      ciNbtcfg.bF2.BRP = 0;
      ciNbtcfg.bF2.TSEG1 = 62;
      wd2 = ciNbtcfg.word;
      ciNbtcfg.word = 0x0F0F;
      ciNbtcfg.bF1.TSEG2 = 15;
      ciNbtcfg.bF1.SJW = 15;
      wd1 = ciNbtcfg.word;
      break;

      // All 250K
    case CAN_250K_500K:
    case CAN_250K_833K:
    case CAN_250K_1M:
    case CAN_250K_1M5:
    case CAN_250K_2M:
    case CAN_250K_3M:
    case CAN_250K_4M:
      ciNbtcfg.word = 0x003E;
      ciNbtcfg.bF2.BRP = 0;
      ciNbtcfg.bF2.TSEG1 = 126;
      wd2 = ciNbtcfg.word;
      ciNbtcfg.word = 0x0F0F;
      ciNbtcfg.bF1.TSEG2 = 31;
      ciNbtcfg.bF1.SJW = 31;
      wd1 = ciNbtcfg.word;
      break;

    case CAN_1000K_4M:
    case CAN_1000K_8M:
      ciNbtcfg.word = 0x003E;
      ciNbtcfg.bF2.BRP = 0;
      ciNbtcfg.bF2.TSEG1 = 30;
      wd2 = ciNbtcfg.word;
      ciNbtcfg.word = 0x0F0F;
      ciNbtcfg.bF1.TSEG2 = 7;
      ciNbtcfg.bF1.SJW = 7;
      wd1 = ciNbtcfg.word;
      break;

    case CAN_125K_500K:
      ciNbtcfg.word = 0x003E;
      ciNbtcfg.bF2.BRP = 0;
      ciNbtcfg.bF2.TSEG1 = 254;
      wd2 = ciNbtcfg.word;
      ciNbtcfg.word = 0x0F0F;
      ciNbtcfg.bF1.TSEG2 = 63;
      ciNbtcfg.bF1.SJW = 63;
      wd1 = ciNbtcfg.word;
      break;

    default:
      return -1;
      break;
    }

  wd = wd1 + (wd2 << 16);
  // Write Bit time registers
  spiTransferError = DRV_CANFDSPI_WriteWord (index, cREGADDR_CiNBTCFG, wd);
  //DRV_CANFDSPI_ReadWord (0,cREGADDR_CiNBTCFG, &u32_test);
  return spiTransferError;
}

int8_t
DRV_CANFDSPI_BitTimeConfigureData40MHz (CANFDSPI_MODULE_ID index,
                                        CAN_BITTIME_SETUP bitTime)
{
  int8_t spiTransferError = 0;
  uint32_t tdcValue = 0;
  uint32_t wdDbtc, wdDbtc1, wdDbtc2;
  REG_CiDBTCFG ciDbtcfg;
  uint32_t wdTDC, wdTDC1, wdTDC2;
  REG_CiTDC ciTdc;
  //    sspMode;

  //ciDbtcfg.word = canControlResetValues[cREGADDR_CiDBTCFG / 4];

  ciTdc.word = 0;
  // Configure Bit time and sample point
  ciTdc.bF2.TDCMode = CAN_SSP_MODE_AUTO;
  wdDbtc2 = ciDbtcfg.word;

  // Data Bit rate and SSP
  switch (bitTime)
    {
    case CAN_500K_1M:
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 30;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 7;
      ciDbtcfg.bF1.SJW = 7;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 31;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_500K_2M:
      // Data BR
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 14;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 3;
      ciDbtcfg.bF1.SJW = 3;
      wdDbtc1 = ciDbtcfg.word;

      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 15;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_500K_3M:
      // Data BR
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 8;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 2;
      ciDbtcfg.bF1.SJW = 2;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 9;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_500K_4M:
    case CAN_1000K_4M:
      // Data BR
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 6;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 1;
      ciDbtcfg.bF1.SJW = 1;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 7;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_500K_5M:
      // Data BR
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 4;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 1;
      ciDbtcfg.bF1.SJW = 1;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 5;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_500K_6M7:
      // Data BR
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 3;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 0;
      ciDbtcfg.bF1.SJW = 0;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 4;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_500K_8M:
    case CAN_1000K_8M:
      // Data BR
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 2;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 0;
      ciDbtcfg.bF1.SJW = 0;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 3;
      ciTdc.bF1.TDCValue = 1;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_500K_10M:
      // Data BR
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 1;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 0;
      ciDbtcfg.bF1.SJW = 0;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 2;
      ciTdc.bF1.TDCValue = 0;
      wdTDC1 = ciTdc.word;
      break;

    case CAN_250K_500K:
    case CAN_125K_500K:
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 1;
      ciDbtcfg.bF2.TSEG1 = 30;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 7;
      ciDbtcfg.bF1.SJW = 7;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 31;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      ciTdc.word = wdDbtc2;
      ciTdc.bF2.TDCMode = CAN_SSP_MODE_OFF;
      wdTDC2 = ciTdc.word;
      break;
    case CAN_250K_833K:
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 1;
      ciDbtcfg.bF2.TSEG1 = 17;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 4;
      ciDbtcfg.bF1.SJW = 4;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 18;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      ciTdc.word = wdDbtc2;
      ciTdc.bF2.TDCMode = CAN_SSP_MODE_OFF;
      wdTDC2 = ciTdc.word;
      break;
    case CAN_250K_1M:
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 30;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 7;
      ciDbtcfg.bF1.SJW = 7;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 31;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_250K_1M5:
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 18;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 5;
      ciDbtcfg.bF1.SJW = 5;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 19;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_250K_2M:
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 14;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 3;
      ciDbtcfg.bF1.SJW = 3;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 15;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_250K_3M:
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 8;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 2;
      ciDbtcfg.bF1.SJW = 2;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 9;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_250K_4M:
      // Data BR
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 6;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 1;
      ciDbtcfg.bF1.SJW = 1;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 7;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;

    default:
      return -1;
      break;
    }

  wdDbtc = wdDbtc1 + (wdDbtc2 << 16);
  // Write Bit time registers
  spiTransferError = DRV_CANFDSPI_WriteWord (index, cREGADDR_CiDBTCFG, wdDbtc);
  if (spiTransferError)
    {
      return -2;
    }

  // Write Transmitter Delay Compensation
#ifdef REV_A
    ciTdc.bF.TDCOffset = 0;
    ciTdc.bF.TDCValue = 0;
#endif

  wdTDC = wdTDC1 + (wdTDC2 << 16);

  spiTransferError = DRV_CANFDSPI_WriteWord (index, cREGADDR_CiTDC, wdTDC);
  if (spiTransferError)
    {
      return -3;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_BitTimeConfigureNominal20MHz (CANFDSPI_MODULE_ID index,
                                           CAN_BITTIME_SETUP bitTime)
{
  int8_t spiTransferError = 0;
  uint32_t wd, wd1, wd2;
  REG_CiNBTCFG ciNbtcfg;

  //ciNbtcfg.word = canControlResetValues[cREGADDR_CiNBTCFG / 4];

  // Arbitration Bit rate
  switch (bitTime)
    {
    // All 500K
    case CAN_500K_1M:
    case CAN_500K_2M:
    case CAN_500K_4M:
    case CAN_500K_5M:
    case CAN_500K_6M7:
    case CAN_500K_8M:
    case CAN_500K_10M:
      ciNbtcfg.word = 0x003E;
      ciNbtcfg.bF2.BRP = 0;
      ciNbtcfg.bF2.TSEG1 = 30;
      wd2 = ciNbtcfg.word;
      ciNbtcfg.word = 0x0F0F;
      ciNbtcfg.bF1.TSEG2 = 7;
      ciNbtcfg.bF1.SJW = 7;
      wd1 = ciNbtcfg.word;
      break;
      // All 250K
    case CAN_250K_500K:
    case CAN_250K_833K:
    case CAN_250K_1M:
    case CAN_250K_1M5:
    case CAN_250K_2M:
    case CAN_250K_3M:
    case CAN_250K_4M:
      ciNbtcfg.word = 0x003E;
      ciNbtcfg.bF2.BRP = 0;
      ciNbtcfg.bF2.TSEG1 = 62;
      wd2 = ciNbtcfg.word;
      ciNbtcfg.word = 0x0F0F;
      ciNbtcfg.bF1.TSEG2 = 15;
      ciNbtcfg.bF1.SJW = 15;
      wd1 = ciNbtcfg.word;
      break;

    case CAN_1000K_4M:
    case CAN_1000K_8M:
      ciNbtcfg.word = 0x003E;
      ciNbtcfg.bF2.BRP = 0;
      ciNbtcfg.bF2.TSEG1 = 14;
      wd2 = ciNbtcfg.word;
      ciNbtcfg.word = 0x0F0F;
      ciNbtcfg.bF1.TSEG2 = 3;
      ciNbtcfg.bF1.SJW = 3;
      wd1 = ciNbtcfg.word;
      break;

    case CAN_125K_500K:
      ciNbtcfg.word = 0x003E;
      ciNbtcfg.bF2.BRP = 0;
      ciNbtcfg.bF2.TSEG1 = 126;
      wd2 = ciNbtcfg.word;
      ciNbtcfg.word = 0x0F0F;
      ciNbtcfg.bF1.TSEG2 = 31;
      ciNbtcfg.bF1.SJW = 31;
      wd1 = ciNbtcfg.word;
      break;

    default:
      return -1;
      break;
    }
  wd = wd1 + (wd2 << 16);
  // Write Bit time registers
  spiTransferError = DRV_CANFDSPI_WriteWord (index, cREGADDR_CiNBTCFG, wd);
  //DRV_CANFDSPI_ReadWord (0, cREGADDR_CiNBTCFG, &u32_test);

  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_BitTimeConfigureData20MHz (CANFDSPI_MODULE_ID index,
                                        CAN_BITTIME_SETUP bitTime)
{
  int8_t spiTransferError = 0;
  uint32_t tdcValue = 0;
  uint32_t wdDbtc, wdDbtc1, wdDbtc2;
  REG_CiDBTCFG ciDbtcfg;
  uint32_t wdTDC, wdTDC1, wdTDC2;
  REG_CiTDC ciTdc;
  //    sspMode;

  //ciDbtcfg.word = canControlResetValues[cREGADDR_CiDBTCFG / 4];
  ciTdc.word = 0;

  // Configure Bit time and sample point
  ciTdc.bF2.TDCMode = CAN_SSP_MODE_AUTO;
  wdTDC2 = ciTdc.word;

  // Data Bit rate and SSP
  switch (bitTime)
    {
    case CAN_500K_1M:
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 14;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 3;
      ciDbtcfg.bF1.SJW = 3;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 15;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_500K_2M:
      // Data BR
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 6;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 1;
      ciDbtcfg.bF1.SJW = 1;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 7;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_500K_4M:
    case CAN_1000K_4M:
      // Data BR
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 2;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 0;
      ciDbtcfg.bF1.SJW = 0;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 3;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_500K_5M:
      // Data BR
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 1;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 0;
      ciDbtcfg.bF1.SJW = 0;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 2;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_500K_6M7:
    case CAN_500K_8M:
    case CAN_500K_10M:
    case CAN_1000K_8M:
      //qDebug("Data Bitrate not feasible with this clock!");
      return -1;
      break;

    case CAN_250K_500K:
    case CAN_125K_500K:
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 30;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 7;
      ciDbtcfg.bF1.SJW = 7;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 31;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      ciTdc.word = wdDbtc2;
      ciTdc.bF2.TDCMode = CAN_SSP_MODE_OFF;
      wdTDC2 = ciTdc.word;
      break;
    case CAN_250K_833K:
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 17;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 4;
      ciDbtcfg.bF1.SJW = 4;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 18;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      ciTdc.word = wdDbtc2;
      ciTdc.bF2.TDCMode = CAN_SSP_MODE_OFF;
      wdTDC2 = ciTdc.word;
      break;
    case CAN_250K_1M:
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 14;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 3;
      ciDbtcfg.bF1.SJW = 3;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 15;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_250K_1M5:
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 8;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 2;
      ciDbtcfg.bF1.SJW = 2;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 9;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_250K_2M:
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 6;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 1;
      ciDbtcfg.bF1.SJW = 1;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 7;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_250K_3M:
      //qDebug("Data Bitrate not feasible with this clock!");
      return -1;
      break;
    case CAN_250K_4M:
      // Data BR
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 2;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 0;
      ciDbtcfg.bF1.SJW = 0;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 3;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;

    default:
      return -1;
      break;
    }

  wdDbtc = wdDbtc1 + (wdDbtc2 << 16);
  // Write Bit time registers
  spiTransferError = DRV_CANFDSPI_WriteWord (index, cREGADDR_CiDBTCFG, wdDbtc);

  if (spiTransferError)
    {
      return -2;
    }

  // Write Transmitter Delay Compensation
#ifdef REV_A
    ciTdc.bF.TDCOffset = 0;
    ciTdc.bF.TDCValue = 0;
#endif

  wdTDC = wdTDC1 + (wdTDC2 << 16);

  spiTransferError = DRV_CANFDSPI_WriteWord (index, cREGADDR_CiTDC, wdTDC);
  //DRV_CANFDSPI_ReadWord (0,cREGADDR_CiTDC, &u32_test);
  if (spiTransferError)
    {
      return -3;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_BitTimeConfigureNominal10MHz (CANFDSPI_MODULE_ID index,
                                           CAN_BITTIME_SETUP bitTime)
{
  int8_t spiTransferError = 0;
  uint32_t wd, wd1, wd2;
  REG_CiNBTCFG ciNbtcfg;

  //ciNbtcfg.word = canControlResetValues[cREGADDR_CiNBTCFG / 4];

  // Arbitration Bit rate
  switch (bitTime)
    {
    // All 500K
    case CAN_500K_1M:
    case CAN_500K_2M:
    case CAN_500K_4M:
    case CAN_500K_5M:
    case CAN_500K_6M7:
    case CAN_500K_8M:
    case CAN_500K_10M:
      ciNbtcfg.word = 0x003E;
      ciNbtcfg.bF2.BRP = 0;
      ciNbtcfg.bF2.TSEG1 = 14;
      wd2 = ciNbtcfg.word;
      ciNbtcfg.word = 0x0F0F;
      ciNbtcfg.bF1.TSEG2 = 3;
      ciNbtcfg.bF1.SJW = 3;
      break;

      // All 250K
    case CAN_250K_500K:
    case CAN_250K_833K:
    case CAN_250K_1M:
    case CAN_250K_1M5:
    case CAN_250K_2M:
    case CAN_250K_3M:
    case CAN_250K_4M:
      ciNbtcfg.word = 0x003E;
      ciNbtcfg.bF2.BRP = 0;
      ciNbtcfg.bF2.TSEG1 = 30;
      wd2 = ciNbtcfg.word;
      ciNbtcfg.word = 0x0F0F;
      ciNbtcfg.bF1.TSEG2 = 7;
      ciNbtcfg.bF1.SJW = 7;
      wd1 = ciNbtcfg.word;
      break;

    case CAN_1000K_4M:
    case CAN_1000K_8M:
      ciNbtcfg.word = 0x003E;
      ciNbtcfg.bF2.BRP = 0;
      ciNbtcfg.bF2.TSEG1 = 7;
      wd2 = ciNbtcfg.word;
      ciNbtcfg.word = 0x0F0F;
      ciNbtcfg.bF1.TSEG2 = 2;
      ciNbtcfg.bF1.SJW = 2;
      wd1 = ciNbtcfg.word;
      break;

    case CAN_125K_500K:
      ciNbtcfg.word = 0x003E;
      ciNbtcfg.bF2.BRP = 0;
      ciNbtcfg.bF2.TSEG1 = 62;
      wd2 = ciNbtcfg.word;
      ciNbtcfg.word = 0x0F0F;
      ciNbtcfg.bF1.TSEG2 = 15;
      ciNbtcfg.bF1.SJW = 15;
      wd1 = ciNbtcfg.word;
      break;

    default:
      return -1;
      break;
    }

  wd = wd1 + (wd2 << 16);
  // Write Bit time registers
  spiTransferError = DRV_CANFDSPI_WriteWord (index, cREGADDR_CiNBTCFG, wd);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_BitTimeConfigureData10MHz (CANFDSPI_MODULE_ID index,
                                        CAN_BITTIME_SETUP bitTime)
{
  int8_t spiTransferError = 0;
  uint32_t tdcValue = 0;
  REG_CiDBTCFG ciDbtcfg;
  uint32_t wdDbtc, wdDbtc1, wdDbtc2;
  REG_CiTDC ciTdc;
  uint32_t wdTDC, wdTDC1, wdTDC2;

  //    sspMode;

  //ciDbtcfg.word = canControlResetValues[cREGADDR_CiDBTCFG / 4];
  ciTdc.word = 0;

  // Configure Bit time and sample point
  ciTdc.bF2.TDCMode = CAN_SSP_MODE_AUTO;
  wdTDC2 = ciTdc.word;

  // Data Bit rate and SSP
  switch (bitTime)
    {
    case CAN_500K_1M:
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 6;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 1;
      ciDbtcfg.bF1.SJW = 1;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 7;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_500K_2M:
      // Data BR
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 2;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 0;
      ciDbtcfg.bF1.SJW = 0;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 3;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_500K_4M:
    case CAN_500K_5M:
    case CAN_500K_6M7:
    case CAN_500K_8M:
    case CAN_500K_10M:
    case CAN_1000K_4M:
    case CAN_1000K_8M:
      //qDebug("Data Bitrate not feasible with this clock!");
      return -1;
      break;

    case CAN_250K_500K:
    case CAN_125K_500K:
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 14;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 3;
      ciDbtcfg.bF1.SJW = 3;
      wdDbtc1 = ciDbtcfg.word;
      // SSP

      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 15;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      ciTdc.word = wdDbtc2;
      ciTdc.bF2.TDCMode = CAN_SSP_MODE_OFF;
      wdTDC2 = ciTdc.word;
      break;
    case CAN_250K_833K:
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 7;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 2;
      ciDbtcfg.bF1.SJW = 2;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 8;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      ciTdc.word = wdDbtc2;
      ciTdc.bF2.TDCMode = CAN_SSP_MODE_OFF;
      wdTDC2 = ciTdc.word;
      break;
    case CAN_250K_1M:
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 6;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 1;
      ciDbtcfg.bF1.SJW = 1;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 7;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_250K_1M5:
      //qDebug("Data Bitrate not feasible with this clock!");
      return -1;
      break;
    case CAN_250K_2M:
      ciDbtcfg.word = 0x000E;
      ciDbtcfg.bF2.BRP = 0;
      ciDbtcfg.bF2.TSEG1 = 2;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
      ciDbtcfg.bF1.TSEG2 = 0;
      ciDbtcfg.bF1.SJW = 0;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 3;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_250K_3M:
    case CAN_250K_4M:
      //qDebug("Data Bitrate not feasible with this clock!");
      return -1;
      break;

    default:
      return -1;
      break;
    }
  wdDbtc = wdDbtc1 + (wdDbtc2 << 16);
  // Write Bit time registers
  spiTransferError = DRV_CANFDSPI_WriteWord (index, cREGADDR_CiDBTCFG,
                                             ciDbtcfg.word);
  if (spiTransferError)
    {
      return -2;
    }

  // Write Transmitter Delay Compensation
#ifdef REV_A
    ciTdc.bF.TDCOffset = 0;
    ciTdc.bF.TDCValue = 0;
#endif

  wdTDC = wdTDC1 + (wdTDC2 << 16);
  spiTransferError = DRV_CANFDSPI_WriteWord (index, cREGADDR_CiTDC, ciTdc.word);
  if (spiTransferError)
    {
      return -3;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_OperationModeSelect (CANFDSPI_MODULE_ID index,
                                  CAN_OPERATION_MODE opMode)
{
  uint8_t d = 0;
  int8_t spiTransferError = 0;

  // Read
  spiTransferError = DRV_CANFDSPI_ReadByte (index, cREGADDR_CiCON + 3, &d);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  d &= ~0x07;
  d |= opMode;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, cREGADDR_CiCON + 3, d);
  //DRV_CANFDSPI_ReadByte (0,cREGADDR_CiCON + 3, &wd);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

CAN_OPERATION_MODE
DRV_CANFDSPI_OperationModeGet (CANFDSPI_MODULE_ID index)
{
  uint8_t d = 0;
  CAN_OPERATION_MODE mode = CAN_INVALID_MODE;
  int8_t spiTransferError = 0;

  // Read Opmode
  spiTransferError = DRV_CANFDSPI_ReadByte (index, cREGADDR_CiCON + 2, &d);
  if (spiTransferError)
    {
      return CAN_INVALID_MODE;
    }

  // Get Opmode bits
  d = (d >> 5) & 0x7;

  // Decode Opmode
  switch (d)
    {
    case CAN_NORMAL_MODE:
      mode = CAN_NORMAL_MODE;
      break;
    case CAN_SLEEP_MODE:
      mode = CAN_SLEEP_MODE;
      break;
    case CAN_INTERNAL_LOOPBACK_MODE:
      mode = CAN_INTERNAL_LOOPBACK_MODE;
      break;
    case CAN_EXTERNAL_LOOPBACK_MODE:
      mode = CAN_EXTERNAL_LOOPBACK_MODE;
      break;
    case CAN_LISTEN_ONLY_MODE:
      mode = CAN_LISTEN_ONLY_MODE;
      break;
    case CAN_CONFIGURATION_MODE:
      mode = CAN_CONFIGURATION_MODE;
      break;
    case CAN_CLASSIC_MODE:
      mode = CAN_CLASSIC_MODE;
      break;
    case CAN_RESTRICTED_MODE:
      mode = CAN_RESTRICTED_MODE;
      break;
    default:
      mode = CAN_INVALID_MODE;
      break;
    }

  return mode;
}

int8_t
DRV_CANFDSPI_LowPowerModeEnable (CANFDSPI_MODULE_ID index)
{
  int8_t spiTransferError = 0;
  uint8_t d = 0;

#ifdef MCP2517FD
    // LPM not implemented
    spiTransferError = -100;
#else
  // Read
  spiTransferError = DRV_CANFDSPI_ReadByte (index, cREGADDR_OSC, &d);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  d |= 0x08;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, cREGADDR_OSC, d);
  if (spiTransferError)
    {
      return -2;
    }
#endif

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_LowPowerModeDisable (CANFDSPI_MODULE_ID index)
{
  int8_t spiTransferError = 0;
  uint8_t d = 0;

#ifdef MCP2517FD
    // LPM not implemented
    spiTransferError = -100;
#else
  // Read
  spiTransferError = DRV_CANFDSPI_ReadByte (index, cREGADDR_OSC, &d);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  d &= ~0x08;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, cREGADDR_OSC, d);
  if (spiTransferError)
    {
      return -2;
    }
#endif

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_FilterObjectConfigure (CANFDSPI_MODULE_ID index, CAN_FILTER filter,
                                    CAN_FILTEROBJ_ID *id)
{
  uint16_t a;
  uint32_t wd, wd1, wd2;
  REG_CiFLTOBJ fObj;
  int8_t spiTransferError = 0;

  // Setup
  fObj.word = 0;
  fObj.bF.bF1 = id->bF1;
  wd1 = (fObj.word) >> 16;
  fObj.word = 0;
  fObj.bF.bF2 = id->bF2;
  wd2 = fObj.word;
  //fObj.word = 0xda;
  a = cREGADDR_CiFLTOBJ + (filter * CiFILTER_OFFSET);

  wd = wd1 + (wd2 << 16);

  spiTransferError = DRV_CANFDSPI_WriteWord (index, a, wd);
  //DRV_CANFDSPI_ReadWord (0,a, &u32_test);
  return spiTransferError;
}

int8_t
DRV_CANFDSPI_FilterMaskConfigure (CANFDSPI_MODULE_ID index, CAN_FILTER filter,
                                  CAN_MASKOBJ_ID *mask)
{
  uint16_t a;
  uint32_t wd, wd1, wd2;
  REG_CiMASK mObj;
  int8_t spiTransferError = 0;

  // Setup
  mObj.word = 0;
  mObj.bF.bF1 = mask->bF1;
  wd1 = (mObj.word) >> 16;
  mObj.word = 0;
  mObj.bF.bF2 = mask->bF2;
  wd2 = mObj.word;
  //mObj.word = 0x40000000;
  a = cREGADDR_CiMASK + (filter * CiFILTER_OFFSET);

  wd = wd1 + (wd2 << 16);

  spiTransferError = DRV_CANFDSPI_WriteWord (index, a, wd);
  //DRV_CANFDSPI_ReadWord (0,a, &u32_test);
  return spiTransferError;
}

int8_t
DRV_CANFDSPI_FilterToFifoLink (CANFDSPI_MODULE_ID index, CAN_FILTER filter,
                               CAN_FIFO_CHANNEL channel, bool enable)
{
  uint16_t a;
  REG_CiFLTCON_BYTE fCtrl;
  int8_t spiTransferError = 0;

  // Enable
  if (enable)
    {
      fCtrl.bF.Enable = 1;
    }
  else
    {
      fCtrl.bF.Enable = 0;
    }

  // Link
  fCtrl.bF.BufferPointer = channel;
  //fCtrl.byte = 129;                    //0x81
  a = cREGADDR_CiFLTCON + filter;

  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, fCtrl.byte);
  //DRV_CANFDSPI_ReadWord (0,a, &u32_test);
  return spiTransferError;
}

int8_t
DRV_CANFDSPI_GpioModeConfigure (CANFDSPI_MODULE_ID index, GPIO_PIN_MODE gpio0,
                                GPIO_PIN_MODE gpio1)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  uint8_t byte;
  REG_IOCON iocon;

  // Read
  a = cREGADDR_IOCON + 3;
  iocon.word = 0;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &iocon.byte[0]);

  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  iocon.bF2.PinMode0 = gpio0;
  iocon.bF2.PinMode1 = gpio1;
  byte = iocon.byte[0];
  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, byte);
  //DRV_CANFDSPI_ReadWord (index, cREGADDR_IOCON, &u32_test);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_GpioDirectionConfigure (CANFDSPI_MODULE_ID index,
                                     GPIO_PIN_DIRECTION gpio0,
                                     GPIO_PIN_DIRECTION gpio1)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  uint8_t byte;
  REG_IOCON iocon;

  // Read
  a = cREGADDR_IOCON;
  iocon.word = 0;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &iocon.byte[1]);

  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  iocon.bF1.TRIS0 = gpio0;
  iocon.bF1.TRIS1 = gpio1;
  byte = iocon.byte[1];
  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, byte);
  //DRV_CANFDSPI_ReadWord (index, cREGADDR_IOCON, &u32_test);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_GpioStandbyControlEnable (CANFDSPI_MODULE_ID index)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  REG_IOCON iocon;
  // Read
  a = cREGADDR_IOCON;

  iocon.word = 0;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &iocon.byte[1]);

  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  iocon.bF1.XcrSTBYEnable = 1;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, iocon.byte[1]);
  //DRV_CANFDSPI_ReadWord (index, cREGADDR_IOCON, &u32_test);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_GpioStandbyControlDisable (CANFDSPI_MODULE_ID index)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  REG_IOCON iocon;
  // Read
  a = cREGADDR_IOCON;
  //REG_IOCON iocon;
  iocon.word = 0;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &iocon.byte[1]);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  iocon.bF1.XcrSTBYEnable = 0;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, iocon.byte[1]);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_GpioInterruptPinsOpenDrainConfigure (CANFDSPI_MODULE_ID index,
                                                  GPIO_OPEN_DRAIN_MODE mode)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  REG_IOCON iocon;
  // Read
  a = cREGADDR_IOCON + 3;
  //REG_IOCON iocon;
  iocon.word = 0;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &iocon.byte[0]);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  iocon.bF2.INTPinOpenDrain = mode;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, iocon.byte[0]);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_GpioTransmitPinOpenDrainConfigure (CANFDSPI_MODULE_ID index,
                                                GPIO_OPEN_DRAIN_MODE mode)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  REG_IOCON iocon;
  // Read
  a = cREGADDR_IOCON + 3;

  iocon.word = 0;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &iocon.byte[0]);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  iocon.bF2.TXCANOpenDrain = mode;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, iocon.byte[0]);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_GpioPinSet (CANFDSPI_MODULE_ID index, GPIO_PIN_POS pos,
                         GPIO_PIN_STATE latch)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  REG_IOCON iocon;
  // Read
  a = cREGADDR_IOCON + 1;

  iocon.word = 0;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &iocon.byte[0]);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  switch (pos)
    {
    case GPIO_PIN_0:
      iocon.bF1.LAT0 = latch;
      break;
    case GPIO_PIN_1:
      iocon.bF1.LAT1 = latch;
      break;
    default:
      return -1;
      break;
    }

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, iocon.byte[0]);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_GpioPinRead (CANFDSPI_MODULE_ID index, GPIO_PIN_POS pos,
                          GPIO_PIN_STATE *state)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  REG_IOCON iocon;
  // Read
  a = cREGADDR_IOCON + 2;

  iocon.word = 0;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &iocon.byte[1]);
  if (spiTransferError)
    {
      return -1;
    }

  // Update data
  switch (pos)
    {
    case GPIO_PIN_0:
      *state = (GPIO_PIN_STATE) iocon.bF2.GPIO0;
      break;
    case GPIO_PIN_1:
      *state = (GPIO_PIN_STATE) iocon.bF2.GPIO1;
      break;
    default:
      return -1;
      break;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_GpioClockOutputConfigure (CANFDSPI_MODULE_ID index,
                                       GPIO_CLKO_MODE mode)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  REG_IOCON iocon;
  // Read
  a = cREGADDR_IOCON + 3;

  iocon.word = 0;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &iocon.byte[0]);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  iocon.bF2.SOFOutputEnable = mode;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, iocon.byte[0]);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ReceiveChannelEventEnable (CANFDSPI_MODULE_ID index,
                                        CAN_FIFO_CHANNEL channel,
                                        CAN_RX_FIFO_EVENT flags)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  REG_CiFIFOCON ciFifoCon;
  if (channel == CAN_TXQUEUE_CH0)
    return -100;

  // Read Interrupt Enables
  a = cREGADDR_CiFIFOCON + (channel * CiFIFO_OFFSET);

  ciFifoCon.word = 0;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &ciFifoCon.byte[1]);

  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  ciFifoCon.byte[1] |= (flags & CAN_RX_FIFO_ALL_EVENTS);

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, ciFifoCon.byte[1]);
  DRV_CANFDSPI_ReadWord (index, a, &u32_test);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ReceiveChannelStatusGet (CANFDSPI_MODULE_ID index,
                                      CAN_FIFO_CHANNEL channel,
                                      CAN_RX_FIFO_STATUS *status)
{
  uint16_t a;
  REG_CiFIFOSTA1 ciFifoSta;
  int8_t spiTransferError = 0;

  // Read
  ciFifoSta.word = 0;
  a = cREGADDR_CiFIFOSTA + (channel * CiFIFO_OFFSET);

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &ciFifoSta.byte[0]);
  if (spiTransferError)
    {
      return -1;
    }

  // Update data
  *status = (CAN_RX_FIFO_STATUS) (ciFifoSta.byte[0] & 0x0F);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ModuleEventGet (CANFDSPI_MODULE_ID index, CAN_MODULE_EVENT *flags)
{
  int8_t spiTransferError = 0;

  // Read Interrupt flags
  REG_CiINTFLAG intFlags;
  intFlags.word = 0;

  spiTransferError = DRV_CANFDSPI_ReadHalfWord (index, cREGADDR_CiINTFLAG,
                                                &intFlags.word);
  if (spiTransferError)
    {
      return -1;
    }

  // Update data
  *flags = (CAN_MODULE_EVENT) (intFlags.word & CAN_ALL_EVENTS);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ModuleEventEnable (CANFDSPI_MODULE_ID index,
                                CAN_MODULE_EVENT flags)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  REG_CiINTENABLE intEnables;
  // Read Interrupt Enables
  a = cREGADDR_CiINTENABLE;

  intEnables.word = 0;

  spiTransferError = DRV_CANFDSPI_ReadHalfWord (index, a, &intEnables.word);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  intEnables.word |= (flags & CAN_ALL_EVENTS);

  // Write
  spiTransferError = DRV_CANFDSPI_WriteHalfWord (index, a, intEnables.word);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ModuleEventDisable (CANFDSPI_MODULE_ID index,
                                 CAN_MODULE_EVENT flags)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  REG_CiINTENABLE intEnables;
  // Read Interrupt Enables
  a = cREGADDR_CiINTENABLE;

  intEnables.word = 0;

  spiTransferError = DRV_CANFDSPI_ReadHalfWord (index, a, &intEnables.word);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  intEnables.word &= ~(flags & CAN_ALL_EVENTS);

  // Write
  spiTransferError = DRV_CANFDSPI_WriteHalfWord (index, a, intEnables.word);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ModuleEventClear (CANFDSPI_MODULE_ID index, CAN_MODULE_EVENT flags)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  REG_CiINTFLAG intFlags;
  // Read Interrupt flags
  a = cREGADDR_CiINTFLAG;

  intFlags.word = 0;

  // Write 1 to all flags except the ones that we want to clear
  // Writing a 1 will not set the flag
  // Only writing a 0 will clear it
  // The flags are HS/C
  intFlags.word = CAN_ALL_EVENTS;
  intFlags.word &= ~flags;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteHalfWord (index, a, intFlags.word);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ModuleEventRxCodeGet (CANFDSPI_MODULE_ID index, CAN_RXCODE *rxCode)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  uint8_t rxCodeByte = 0;

  // Read
  a = cREGADDR_CiVEC + 3;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &rxCodeByte);
  if (spiTransferError)
    {
      return -1;
    }

  // Decode data
  // 0x40 = "no interrupt" (CAN_FIFO_CIVEC_NOINTERRUPT)
  if ((rxCodeByte < CAN_RXCODE_TOTAL_CHANNELS)
      || (rxCodeByte == CAN_RXCODE_NO_INT))
    {
      *rxCode = (CAN_RXCODE) rxCodeByte;
    }
  else
    {
      *rxCode = CAN_RXCODE_RESERVED; // shouldn't get here
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ModuleEventTxCodeGet (CANFDSPI_MODULE_ID index, CAN_TXCODE *txCode)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  uint8_t txCodeByte = 0;

  // Read
  a = cREGADDR_CiVEC + 2;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &txCodeByte);
  if (spiTransferError)
    {
      return -1;
    }

  // Decode data
  // 0x40 = "no interrupt" (CAN_FIFO_CIVEC_NOINTERRUPT)
  if ((txCodeByte < CAN_TXCODE_TOTAL_CHANNELS)
      || (txCodeByte == CAN_TXCODE_NO_INT))
    {
      *txCode = (CAN_TXCODE) txCodeByte;
    }
  else
    {
      *txCode = CAN_TXCODE_RESERVED; // shouldn't get here
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ModuleEventFilterHitGet (CANFDSPI_MODULE_ID index,
                                      CAN_FILTER *filterHit)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  uint8_t filterHitByte = 0;

  // Read
  a = cREGADDR_CiVEC + 1;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &filterHitByte);
  if (spiTransferError)
    {
      return -1;
    }

  // Update data
  *filterHit = (CAN_FILTER) filterHitByte;

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ModuleEventIcodeGet (CANFDSPI_MODULE_ID index, CAN_ICODE *icode)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  uint8_t icodeByte = 0;

  // Read
  a = cREGADDR_CiVEC;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &icodeByte);
  if (spiTransferError)
    {
      return -1;
    }

  // Decode
  if ((icodeByte < CAN_ICODE_RESERVED)
      && ((icodeByte < CAN_ICODE_TOTAL_CHANNELS)
          || (icodeByte >= CAN_ICODE_NO_INT)))
    {
      *icode = (CAN_ICODE) icodeByte;
    }
  else
    {
      *icode = CAN_ICODE_RESERVED; // shouldn't get here
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ReceiveChannelEventGet (CANFDSPI_MODULE_ID index,
                                     CAN_FIFO_CHANNEL channel,
                                     CAN_RX_FIFO_EVENT *flags)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  REG_CiFIFOSTA1 ciFifoSta;
  if (channel == CAN_TXQUEUE_CH0)
    return -100;

  // Read Interrupt flags

  ciFifoSta.word = 0;
  a = cREGADDR_CiFIFOSTA + (channel * CiFIFO_OFFSET);

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &ciFifoSta.byte[0]);

  if (spiTransferError)
    {
      return -1;
    }

  // Update data
  *flags = (CAN_RX_FIFO_EVENT) (ciFifoSta.byte[0] & CAN_RX_FIFO_ALL_EVENTS);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ReceiveMessageGet (CANFDSPI_MODULE_ID index,
                                CAN_FIFO_CHANNEL channel, CAN_RX_MSGOBJ *rxObj,
                                uint8_t *rxd, uint8_t nBytes)
{
  uint8_t *u8p;
  uint8_t n = 0;
  uint8_t i = 0;
  uint8_t ba[MAX_MSG_SIZE];
  REG_t myReg;
  uint16_t a;
  uint32_t fifoReg[3];
  REG_CiFIFOCON1 ciFifoCon;
  REG_CiFIFOSTA1 ciFifoSta;
  REG_CiFIFOUA1 ciFifoUa;
  int8_t spiTransferError = 0;

  // Get FIFO registers
  a = cREGADDR_CiFIFOCON + (channel * CiFIFO_OFFSET);

  spiTransferError = DRV_CANFDSPI_ReadWordArray (index, a, fifoReg, 3);
  if (spiTransferError)
    {
      return -1;
    }

  // Check that it is a receive buffer
  ciFifoCon.word = fifoReg[0];
  u8p = (uint8_t*) &ciFifoCon.word;
  if (ciFifoCon.byte[0] & 0x80)
    {
      return -2;
    }

// Get Status
  ciFifoSta.word = fifoReg[1];

  // Get address
  ciFifoUa.word = fifoReg[2];
#ifdef USERADDRESS_TIMES_FOUR
    a = 4 * ciFifoUa.bF.UserAddress;
#else
  a = ciFifoUa.byte[0] + ((((uint16_t) ciFifoUa.byte[1]) << 8) & 0xF00);
#endif
  a += cRAMADDR_START;

  // Number of bytes to read
  n = nBytes + 8; // Add 8 header bytes

  if ((ciFifoCon.byte[0] >> 5) & 0x1)
    {
      n += 4; // Add 4 time stamp bytes
    }

  // Make sure we read a multiple of 4 bytes from RAM
  if (n % 4)
    {
      n = n + 4 - (n % 4);
    }

  // Read rxObj using one access

  if (n > MAX_MSG_SIZE)
    {
      n = MAX_MSG_SIZE;
    }

  spiTransferError = DRV_CANFDSPI_ReadByteArray (index, a, ba, n);
  if (spiTransferError)
    {
      return -3;
    }

  // Assign message header

  myReg.byte[0] = ba[0];
  myReg.byte[1] = ba[1];
  myReg.byte[2] = ba[2];
  myReg.byte[3] = ba[3];
  rxObj->word[0] = myReg.word;

  myReg.byte[0] = ba[4];
  myReg.byte[1] = ba[5];
  myReg.byte[2] = ba[6];
  myReg.byte[3] = ba[7];
  rxObj->word[1] = myReg.word;

//  if ((ciFifoCon.word >> 5) & 0x1)
  if ((ciFifoCon.byte[0] >> 5) & 0x1)
    {
      myReg.byte[0] = ba[8];
      myReg.byte[1] = ba[9];
      myReg.byte[2] = ba[10];
      myReg.byte[3] = ba[11];
      rxObj->word[2] = myReg.word;

      // Assign message data
      for (i = 0; i < nBytes; i++)
        {
          rxd[i] = ba[i + 12];
        }
    }
  else
    {
      rxObj->word[2] = 0;

      // Assign message data
      for (i = 0; i < nBytes; i++)
        {
          rxd[i] = ba[i + 8];
        }
    }

  // UINC channel
  spiTransferError = DRV_CANFDSPI_ReceiveChannelUpdate (index, channel);
  if (spiTransferError)
    {
      return -4;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ReceiveChannelUpdate (CANFDSPI_MODULE_ID index,
                                   CAN_FIFO_CHANNEL channel)
{
  uint16_t a = 0;
  REG_CiFIFOCON ciFifoCon;
  int8_t spiTransferError = 0;
  ciFifoCon.word = 0;

  // Set UINC
  a = cREGADDR_CiFIFOCON + (channel * CiFIFO_OFFSET) + 1; // Byte that contains FRESET
  ciFifoCon.rxBF1.UINC = 1;
  //ciFifoCon.word = 0x100;
  // Write byte
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, ciFifoCon.byte[0]);
  //spiTransferError = DRV_CANFDSPI_WriteByte (index, a, 1);
  //DRV_CANFDSPI_ReadWord (0, cREGADDR_CiFIFOCON, &u32_test);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_TransmitChannelConfigure (CANFDSPI_MODULE_ID index,
                                       CAN_FIFO_CHANNEL channel,
                                       CAN_TX_FIFO_CONFIG *config)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  uint32_t wd, wd1, wd2;

  // Setup FIFO
  REG_CiFIFOCON ciFifoCon;
  ciFifoCon.word = 0x0400;
  ciFifoCon.txBF1.TxEnable = 1;
  ciFifoCon.txBF1.RTREnable = config->RTREnable;
  wd1 = ciFifoCon.word;
  ciFifoCon.word = 0x0060;
  ciFifoCon.txBF2.FifoSize = config->FifoSize;
  ciFifoCon.txBF2.PayLoadSize = config->PayLoadSize;
  ciFifoCon.txBF2.TxAttempts = config->TxAttempts;
  ciFifoCon.txBF2.TxPriority = config->TxPriority;
  wd2 = ciFifoCon.word;

  a = cREGADDR_CiFIFOCON + (channel * CiFIFO_OFFSET);

  wd = wd1 + (wd2 << 16);
  // ciFifoCon.word = 0xe7610480;
  spiTransferError = DRV_CANFDSPI_WriteWord (index, a, wd);
  //DRV_CANFDSPI_ReadWord(index,a,&u32_test);
  return spiTransferError;
}

int8_t
DRV_CANFDSPI_TransmitChannelConfigureObjectReset (CAN_TX_FIFO_CONFIG *config)
{
  REG_CiFIFOCON ciFifoCon;

  ciFifoCon.word = 0x0400;
  config->RTREnable = ciFifoCon.txBF1.RTREnable;
  ciFifoCon.word = 0x0060;
  config->TxPriority = ciFifoCon.txBF2.TxPriority;
  config->TxAttempts = ciFifoCon.txBF2.TxAttempts;
  config->FifoSize = ciFifoCon.txBF2.FifoSize;
  config->PayLoadSize = ciFifoCon.txBF2.PayLoadSize;

  return 0;
}

int8_t
DRV_CANFDSPI_TransmitQueueConfigure (CANFDSPI_MODULE_ID index,
                                     CAN_TX_QUEUE_CONFIG *config)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  uint32_t wd, wd1, wd2;
  // Setup FIFO
  REG_CiTXQCON ciFifoCon;
  ciFifoCon.word = 0x0400;

  ciFifoCon.txBF1.TxEnable = 1;
  wd1 = ciFifoCon.word;
  ciFifoCon.word = 0x0060;
  ciFifoCon.txBF2.FifoSize = config->FifoSize;
  ciFifoCon.txBF2.PayLoadSize = config->PayLoadSize;
  ciFifoCon.txBF2.TxAttempts = config->TxAttempts;
  ciFifoCon.txBF2.TxPriority = config->TxPriority;
  wd2 = ciFifoCon.word;
  a = cREGADDR_CiTXQCON;

  wd = wd1 + (wd2 << 16);

  spiTransferError = DRV_CANFDSPI_WriteWord (index, a, wd);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_TransmitQueueConfigureObjectReset (CAN_TX_QUEUE_CONFIG *config)
{
  REG_CiFIFOCON ciFifoCon;
  ciFifoCon.word = 0x0060;
  config->TxPriority = ciFifoCon.txBF2.TxPriority;
  config->TxAttempts = ciFifoCon.txBF2.TxAttempts;
  config->FifoSize = ciFifoCon.txBF2.FifoSize;
  config->PayLoadSize = ciFifoCon.txBF2.PayLoadSize;

  return 0;
}

int8_t
DRV_CANFDSPI_TransmitChannelLoad (CANFDSPI_MODULE_ID index,
                                  CAN_FIFO_CHANNEL channel,
                                  CAN_TX_MSGOBJ *txObj, uint8_t *txd,
                                  uint32_t txdNumBytes, bool flush)
{
  uint16_t a;
  uint32_t fifoReg[3];
  uint32_t dataBytesInObject;
  REG_CiFIFOCON ciFifoCon;
  REG_CiFIFOSTA1 ciFifoSta;
  REG_CiFIFOUA1 ciFifoUa;
  int8_t spiTransferError = 0;
  uint8_t txBuffer[MAX_MSG_SIZE];
  uint8_t i;
  uint16_t n = 0;
  uint8_t j = 0;

  // Get FIFO registers
  a = cREGADDR_CiFIFOCON + (channel * CiFIFO_OFFSET);

  spiTransferError = DRV_CANFDSPI_ReadWordArray (index, a, fifoReg, 3);
  if (spiTransferError)
    {
      return -1;
    }

  // Check that it is a transmit buffer
  ciFifoCon.word = (uint16_t) fifoReg[0];
  if (!ciFifoCon.txBF1.TxEnable)
    {
      return -2;
    }

  // Check that DLC is big enough for data
  dataBytesInObject = DRV_CANFDSPI_DlcToDataBytes (
      (CAN_DLC) txObj->bF.ctrl.bF1.DLC);
  if (dataBytesInObject < txdNumBytes)
    {
      return -3;
    }

  // Get status
  ciFifoSta.word = fifoReg[1];

  // Get address
  ciFifoUa.word = fifoReg[2];
#ifdef USERADDRESS_TIMES_FOUR
    a = 4 * ciFifoUa.bF.UserAddress;
#else
  a = ciFifoUa.byte[0] + ((((uint16_t) ciFifoUa.byte[1]) << 8) & 0xF00);
#endif
  a += cRAMADDR_START;

  txBuffer[0] = txObj->byte[1]; //not using 'for' to reduce no of instructions
  txBuffer[1] = txObj->byte[0];
  txBuffer[2] = txObj->byte[3];
  txBuffer[3] = txObj->byte[2];

  txBuffer[4] = txObj->byte[5];
  txBuffer[5] = txObj->byte[4];
  txBuffer[6] = txObj->byte[7];
  txBuffer[7] = txObj->byte[6];

  for (i = 0; i < txdNumBytes; i++)
    {
      txBuffer[i + 8] = txd[i];
    }

  // Make sure we write a multiple of 4 bytes to RAM

  if (txdNumBytes % 4)
    {
      // Need to add bytes
      n = 4 - (txdNumBytes % 4);
      i = txdNumBytes + 8;

      for (j = 0; j < n; j++)
        {
          txBuffer[i + 8 + j] = 0;
        }
    }

  spiTransferError = DRV_CANFDSPI_WriteByteArray (index, a, txBuffer,
                                                  txdNumBytes + 8 + n);
  if (spiTransferError)
    {
      return -4;
    }

  // Set UINC and TXREQ
  spiTransferError = DRV_CANFDSPI_TransmitChannelUpdate (index, channel, flush);
  if (spiTransferError)
    {
      return -5;
    }

  return spiTransferError;
}


int8_t
DRV_CANFDSPI_TransmitChannelUpdate (CANFDSPI_MODULE_ID index,
                                    CAN_FIFO_CHANNEL channel, bool flush)
{
  uint16_t a;
  REG_CiFIFOCON ciFifoCon;
  int8_t spiTransferError = 0;

  // Set UINC
  a = cREGADDR_CiFIFOCON + (channel * CiFIFO_OFFSET) + 1; // Byte that contains FRESET
  ciFifoCon.word = 0;
  ciFifoCon.txBF1.UINC = 1;

  // Set TXREQ
  if (flush)
    {
      ciFifoCon.txBF1.TxRequest = 1;
    }

  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, ciFifoCon.byte[1]);
  //DRV_CANFDSPI_ReadWord (0,cREGADDR_CiFIFOCON, &u32_test);

  if (spiTransferError)
    {
      return -1;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_TransmitChannelFlush (CANFDSPI_MODULE_ID index,
                                   CAN_FIFO_CHANNEL channel)
{
  uint8_t wd,d = 0;
  uint16_t a = 0;

  int8_t spiTransferError = 0;

  // Address of TXREQ
  a = cREGADDR_CiFIFOCON + (channel * CiFIFO_OFFSET);
  a += 1;

  // Set TXREQ
  d = 0x02;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, d);
  //DRV_CANFDSPI_ReadByte (0,a, &wd);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_TransmitChannelStatusGet (CANFDSPI_MODULE_ID index,
                                       CAN_FIFO_CHANNEL channel,
                                       CAN_TX_FIFO_STATUS *status)
{
  uint16_t a = 0;
  uint32_t sta = 0;
  uint32_t fifoReg[2];
  REG_CiFIFOSTA1 ciFifoSta;
  REG_CiFIFOCON ciFifoCon;
  int8_t spiTransferError = 0;

  // Get FIFO registers
  a = cREGADDR_CiFIFOCON + (channel * CiFIFO_OFFSET);

  spiTransferError = DRV_CANFDSPI_ReadWordArray (index, a, fifoReg, 2);
  if (spiTransferError)
    {
      return -1;
    }

  // Update data
  ciFifoCon.word = fifoReg[0];
  ciFifoSta.word = fifoReg[1];

  // Update status
  sta = ciFifoSta.byte[1];

  if (ciFifoCon.txBF1.TxRequest)
    {
      sta |= CAN_TX_FIFO_TRANSMITTING;
    }

  *status = (CAN_TX_FIFO_STATUS) (sta & CAN_TX_FIFO_STATUS_MASK);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_TransmitChannelReset (CANFDSPI_MODULE_ID index,
                                   CAN_FIFO_CHANNEL channel)
{
  return DRV_CANFDSPI_ReceiveChannelReset (index, channel);
}

int8_t
DRV_CANFDSPI_TransmitRequestSet (CANFDSPI_MODULE_ID index,
                                 CAN_TXREQ_CHANNEL txreq)
{
  int8_t spiTransferError = 0;

  // Write TXREQ register
  uint32_t w = txreq;

  spiTransferError = DRV_CANFDSPI_WriteWord (index, cREGADDR_CiTXREQ, w);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_TransmitRequestGet (CANFDSPI_MODULE_ID index, uint32_t *txreq)
{
  int8_t spiTransferError = 0;

  spiTransferError = DRV_CANFDSPI_ReadWord (index, cREGADDR_CiTXREQ, txreq);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_TransmitChannelAbort (CANFDSPI_MODULE_ID index,
                                   CAN_FIFO_CHANNEL channel)
{
  uint16_t a;
  uint8_t d;
  int8_t spiTransferError = 0;

  // Address
  a = cREGADDR_CiFIFOCON + (channel * CiFIFO_OFFSET);
  a += 1; // byte address of TXREQ

  // Clear TXREQ
  d = 0x00;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, d);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_TransmitAbortAll (CANFDSPI_MODULE_ID index)
{
  uint8_t d;
  int8_t spiTransferError = 0;

  // Read CiCON byte 3
  spiTransferError = DRV_CANFDSPI_ReadByte (index, (cREGADDR_CiCON + 3), &d);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  d |= 0x8;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, (cREGADDR_CiCON + 3), d);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_TransmitBandWidthSharingSet (CANFDSPI_MODULE_ID index,
                                          CAN_TX_BANDWITH_SHARING txbws)
{
  uint8_t d = 0;
  int8_t spiTransferError = 0;

  // Read CiCON byte 3
  spiTransferError = DRV_CANFDSPI_ReadByte (index, (cREGADDR_CiCON + 3), &d);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  d &= 0x0f;
  d |= (txbws << 4);

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, (cREGADDR_CiCON + 3), d);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_TefEventGet (CANFDSPI_MODULE_ID index, CAN_TEF_FIFO_EVENT *flags)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;

  // Read Interrupt flags
  REG_CiTEFSTA ciTefSta;
  ciTefSta.word = 0;
  a = cREGADDR_CiTEFSTA;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &ciTefSta.byte[1]);
  if (spiTransferError)
    {
      return -1;
    }

  // Update data
  *flags = (CAN_TEF_FIFO_EVENT) (ciTefSta.byte[1] & CAN_TEF_FIFO_ALL_EVENTS);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_TefEventEnable (CANFDSPI_MODULE_ID index, CAN_TEF_FIFO_EVENT flags)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  REG_CiTEFCON ciTefCon;
  // Read Interrupt Enables
  a = cREGADDR_CiTEFCON;

  ciTefCon.word = 0;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &ciTefCon.byte[1]);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  ciTefCon.byte[1] |= (flags & CAN_TEF_FIFO_ALL_EVENTS);

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, ciTefCon.byte[1]);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_TefEventDisable (CANFDSPI_MODULE_ID index,
                              CAN_TEF_FIFO_EVENT flags)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  REG_CiTEFCON ciTefCon;
  // Read Interrupt Enables
  a = cREGADDR_CiTEFCON;

  ciTefCon.word = 0;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &ciTefCon.byte[1]);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  ciTefCon.byte[1] &= ~(flags & CAN_TEF_FIFO_ALL_EVENTS);

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, ciTefCon.byte[1]);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_TefEventOverflowClear (CANFDSPI_MODULE_ID index)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;

  // Read Interrupt Flags
  REG_CiTEFSTA ciTefSta;
  ciTefSta.word = 0;
  a = cREGADDR_CiTEFSTA;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &ciTefSta.byte[1]);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  ciTefSta.byte[1] &= ~(CAN_TEF_FIFO_OVERFLOW_EVENT);

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, ciTefSta.byte[1]);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_FilterDisable (CANFDSPI_MODULE_ID index, CAN_FILTER filter)
{
  uint16_t a;
  REG_CiFLTCON_BYTE fCtrl;
  int8_t spiTransferError = 0;

  // Read
  a = cREGADDR_CiFLTCON + filter;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &fCtrl.byte);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  fCtrl.bF.Enable = 0;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, fCtrl.byte);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ReceiveChannelReset (CANFDSPI_MODULE_ID index,
                                  CAN_FIFO_CHANNEL channel)
{
  uint16_t a = 0;
  REG_CiFIFOCON ciFifoCon;
  int8_t spiTransferError = 0;

  // Address and data
  a = cREGADDR_CiFIFOCON + (channel * CiFIFO_OFFSET) + 1; // Byte that contains FRESET
  ciFifoCon.word = 0;
  ciFifoCon.rxBF1.FRESET = 1;

  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, ciFifoCon.byte[0]);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_DeviceNetFilterCountSet (CANFDSPI_MODULE_ID index,
                                      CAN_DNET_FILTER_SIZE dnfc)
{
  uint8_t d = 0;
  int8_t spiTransferError = 0;

  // Read CiCON byte 0
  spiTransferError = DRV_CANFDSPI_ReadByte (index, cREGADDR_CiCON, &d);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  d &= 0x1f;
  d |= dnfc;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, cREGADDR_CiCON, d);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ReceiveEventGet (CANFDSPI_MODULE_ID index, uint32_t *rxif)
{
  int8_t spiTransferError = 0;

  spiTransferError = DRV_CANFDSPI_ReadWord (index, cREGADDR_CiRXIF, rxif);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ReceiveEventOverflowGet (CANFDSPI_MODULE_ID index,
                                      uint32_t *rxovif)
{
  int8_t spiTransferError = 0;

  spiTransferError = DRV_CANFDSPI_ReadWord (index, cREGADDR_CiRXOVIF, rxovif);

  return spiTransferError;
}

// *****************************************************************************
// *****************************************************************************
// Section: Error Handling

int8_t
DRV_CANFDSPI_ErrorCountTransmitGet (CANFDSPI_MODULE_ID index, uint8_t *tec)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;

  // Read Error count
  a = cREGADDR_CiTREC + 1;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, tec);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ErrorCountReceiveGet (CANFDSPI_MODULE_ID index, uint8_t *rec)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;

  // Read Error count
  a = cREGADDR_CiTREC;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, rec);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ErrorStateGet (CANFDSPI_MODULE_ID index, CAN_ERROR_STATE *flags)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  uint8_t f = 0;
  // Read Error state
  a = cREGADDR_CiTREC + 2;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &f);
  if (spiTransferError)
    {
      return -1;
    }

  // Update data
  *flags = (CAN_ERROR_STATE) (f & CAN_ERROR_ALL);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ErrorCountStateGet (CANFDSPI_MODULE_ID index, uint8_t *tec,
                                 uint8_t *rec, CAN_ERROR_STATE *flags)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  REG_CiTREC ciTrec;
  // Read Error
  a = cREGADDR_CiTREC;

  ciTrec.word = 0;

  spiTransferError = DRV_CANFDSPI_ReadWord (index, a, &ciTrec.word);
  if (spiTransferError)
    {
      return -1;
    }

  // Update data
  *tec = ciTrec.byte[1];
  *rec = ciTrec.byte[0];
  *flags = (CAN_ERROR_STATE) (ciTrec.byte[2] & CAN_ERROR_ALL);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_BusDiagnosticsGet (CANFDSPI_MODULE_ID index,
                                CAN_BUS_DIAGNOSTIC *bd)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  uint32_t w[2];
  CAN_BUS_DIAGNOSTIC b;
  // Read diagnostic registers all in one shot
  a = cREGADDR_CiBDIAG0;

  spiTransferError = DRV_CANFDSPI_ReadWordArray (index, a, w, 2);
  if (spiTransferError)
    {
      return -1;
    }

  // Update data

  b.word[0] = w[0];
  b.word[1] = w[1];
  *bd = b;

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_BusDiagnosticsClear (CANFDSPI_MODULE_ID index)
{
  int8_t spiTransferError = 0;
  uint8_t a = 0;
  uint32_t w[2];
  // Clear diagnostic registers all in one shot
  a = cREGADDR_CiBDIAG0;

  w[0] = 0;
  w[1] = 0;

  spiTransferError = DRV_CANFDSPI_WriteWordArray (index, a, w, 2);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_EccDisable (CANFDSPI_MODULE_ID index)
{
  int8_t spiTransferError = 0;
  uint8_t d = 0;

  // Read
  spiTransferError = DRV_CANFDSPI_ReadByte (index, cREGADDR_ECCCON, &d);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  d &= ~0x01;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, cREGADDR_ECCCON, d);
  if (spiTransferError)
    {
      return -2;
    }

  return 0;
}

int8_t
DRV_CANFDSPI_EccEventGet (CANFDSPI_MODULE_ID index, CAN_ECC_EVENT *flags)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;

  // Read Interrupt flags
  uint8_t eccStatus = 0;
  a = cREGADDR_ECCSTA;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &eccStatus);
  if (spiTransferError)
    {
      return -1;
    }

  // Update data
  *flags = (CAN_ECC_EVENT) (eccStatus & CAN_ECC_ALL_EVENTS);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_EccParitySet (CANFDSPI_MODULE_ID index, uint8_t parity)
{
  int8_t spiTransferError = 0;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, cREGADDR_ECCCON + 1,
                                             parity);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_EccParityGet (CANFDSPI_MODULE_ID index, uint8_t *parity)
{
  int8_t spiTransferError = 0;

  // Read
  spiTransferError = DRV_CANFDSPI_ReadByte (index, cREGADDR_ECCCON + 1, parity);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_EccErrorAddressGet (CANFDSPI_MODULE_ID index, uint16_t *a)
{
  int8_t spiTransferError = 0;
  uint32_t *wd;
  REG_ECCSTA reg;

  // Read
  spiTransferError = DRV_CANFDSPI_ReadWord (index, cREGADDR_ECCSTA, wd);
  if (spiTransferError)
    {
      return -1;
    }

  reg.word = *wd >> 15;
  // Update data
  *a = reg.bF2.ErrorAddress;

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_EccEventEnable (CANFDSPI_MODULE_ID index, CAN_ECC_EVENT flags)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  uint8_t eccInterrupts = 0;
  // Read
  a = cREGADDR_ECCCON;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &eccInterrupts);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  eccInterrupts |= (flags & CAN_ECC_ALL_EVENTS);

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, eccInterrupts);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_EccEventDisable (CANFDSPI_MODULE_ID index, CAN_ECC_EVENT flags)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  uint8_t eccInterrupts = 0;
  // Read
  a = cREGADDR_ECCCON;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &eccInterrupts);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  eccInterrupts &= ~(flags & CAN_ECC_ALL_EVENTS);

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, eccInterrupts);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_EccEventClear (CANFDSPI_MODULE_ID index, CAN_ECC_EVENT flags)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  uint8_t eccStat = 0;
  // Read
  a = cREGADDR_ECCSTA;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &eccStat);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  eccStat &= ~(flags & CAN_ECC_ALL_EVENTS);

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, eccStat);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

// *****************************************************************************
// *****************************************************************************
// Section: CRC

int8_t
DRV_CANFDSPI_CrcEventEnable (CANFDSPI_MODULE_ID index, CAN_CRC_EVENT flags)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  uint8_t crc;
  // Read interrupt control bits of CRC Register
  a = cREGADDR_CRC + 3;
  //uint8_t crc;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &crc);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  crc |= (flags & CAN_CRC_ALL_EVENTS);

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, crc);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_CrcEventDisable (CANFDSPI_MODULE_ID index, CAN_CRC_EVENT flags)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  uint8_t crc;
  // Read interrupt control bits of CRC Register
  a = cREGADDR_CRC + 3;
  // uint8_t crc;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &crc);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  crc &= ~(flags & CAN_CRC_ALL_EVENTS);

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, crc);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_CrcEventClear (CANFDSPI_MODULE_ID index, CAN_CRC_EVENT flags)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  uint8_t crc;
  // Read interrupt flags of CRC Register
  a = cREGADDR_CRC + 2;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &crc);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  crc &= ~(flags & CAN_CRC_ALL_EVENTS);

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, crc);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_CrcEventGet (CANFDSPI_MODULE_ID index, CAN_CRC_EVENT *flags)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  uint8_t crc;
  // Read interrupt flags of CRC Register
  a = cREGADDR_CRC + 2;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &crc);
  if (spiTransferError)
    {
      return -1;
    }

  // Update data
  *flags = (CAN_CRC_EVENT) (crc & CAN_CRC_ALL_EVENTS);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_CrcValueGet (CANFDSPI_MODULE_ID index, uint16_t *crc)
{
  int8_t spiTransferError = 0;

  // Read CRC value from CRC Register
  spiTransferError = DRV_CANFDSPI_ReadHalfWord (index, cREGADDR_CRC, crc);

  return spiTransferError;
}

// *****************************************************************************
// *****************************************************************************
// Section: Time Stamp

int8_t
DRV_CANFDSPI_TimeStampEnable (CANFDSPI_MODULE_ID index)
{
  int8_t spiTransferError = 0;
  uint8_t d = 0;

  // Read
  spiTransferError = DRV_CANFDSPI_ReadByte (index, cREGADDR_CiTSCON + 2, &d);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  d |= 0x01;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, cREGADDR_CiTSCON + 2, d);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_TimeStampDisable (CANFDSPI_MODULE_ID index)
{
  int8_t spiTransferError = 0;
  uint8_t d = 0;

  // Read
  spiTransferError = DRV_CANFDSPI_ReadByte (index, cREGADDR_CiTSCON + 2, &d);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  d &= 0x06;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, cREGADDR_CiTSCON + 2, d);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_TimeStampGet (CANFDSPI_MODULE_ID index, uint32_t *ts)
{
  int8_t spiTransferError = 0;

  // Read
  spiTransferError = DRV_CANFDSPI_ReadWord (index, cREGADDR_CiTBC, ts);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_TimeStampSet (CANFDSPI_MODULE_ID index, uint32_t ts)
{
  int8_t spiTransferError = 0;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteWord (index, cREGADDR_CiTBC, ts);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_TimeStampModeConfigure (CANFDSPI_MODULE_ID index, CAN_TS_MODE mode)
{
  int8_t spiTransferError = 0;
  uint8_t d = 0;

  // Read
  spiTransferError = DRV_CANFDSPI_ReadByte (index, cREGADDR_CiTSCON + 2, &d);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  d &= 0x01;
  d |= mode << 1;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, cREGADDR_CiTSCON + 2, d);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_TimeStampPrescalerSet (CANFDSPI_MODULE_ID index, uint16_t ps)
{
  int8_t spiTransferError = 0;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteHalfWord (index, cREGADDR_CiTSCON, ps);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_OscillatorEnable (CANFDSPI_MODULE_ID index)
{
  int8_t spiTransferError = 0;
  uint8_t d = 0;

  // Read
  spiTransferError = DRV_CANFDSPI_ReadByte (index, cREGADDR_OSC, &d);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  d &= ~0x4;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, cREGADDR_OSC, d);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_OscillatorControlSet (CANFDSPI_MODULE_ID index, CAN_OSC_CTRL ctrl)
{
  int8_t spiTransferError = 0;

  REG_OSC osc;
  osc.word = 0;

  osc.bF1.PllEnable = ctrl.PllEnable;
  osc.bF1.OscDisable = ctrl.OscDisable;
  osc.bF1.SCLKDIV = ctrl.SclkDivide;
  osc.bF1.CLKODIV = ctrl.ClkOutDivide;
  DRV_CANFDSPI_ReadWord (0,cREGADDR_OSC, &u32_test);
  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, cREGADDR_OSC, osc.byte[0]);


  return spiTransferError;
}

int8_t
DRV_CANFDSPI_OscillatorControlObjectReset (CAN_OSC_CTRL *ctrl)
{
  REG_OSC osc;
  osc.word = 0x0460;

  ctrl->PllEnable = osc.bF1.PllEnable;
  ctrl->OscDisable = osc.bF1.OscDisable;
  ctrl->SclkDivide = osc.bF1.SCLKDIV;
  ctrl->ClkOutDivide = osc.bF1.CLKODIV;

  return 0;
}

int8_t
DRV_CANFDSPI_OscillatorStatusGet (CANFDSPI_MODULE_ID index,
                                  CAN_OSC_STATUS *status)
{
  int8_t spiTransferError = 0;
  CAN_OSC_STATUS stat;
  REG_OSC osc;
  osc.word = 0;

  // Read
  spiTransferError = DRV_CANFDSPI_ReadByte (index, cREGADDR_OSC + 1,
                                            &osc.byte[1]);
  if (spiTransferError)
    {
      return -1;
    }

  stat.PllReady = osc.bF1.PllReady;
  stat.OscReady = osc.bF1.OscReady;
  stat.SclkReady = osc.bF1.SclkReady;

  *status = stat;

  return spiTransferError;
}

uint32_t
DRV_CANFDSPI_DlcToDataBytes (CAN_DLC dlc)
{
  uint32_t dataBytesInObject = 0;

  //Nop();
  //Nop();

  if (dlc < CAN_DLC_12)
    {
      dataBytesInObject = dlc;
    }
  else
    {
      switch (dlc)
        {
        case CAN_DLC_12:
          dataBytesInObject = 12;
          break;
        case CAN_DLC_16:
          dataBytesInObject = 16;
          break;
        case CAN_DLC_20:
          dataBytesInObject = 20;
          break;
        case CAN_DLC_24:
          dataBytesInObject = 24;
          break;
        case CAN_DLC_32:
          dataBytesInObject = 32;
          break;
        case CAN_DLC_48:
          dataBytesInObject = 48;
          break;
        case CAN_DLC_64:
          dataBytesInObject = 64;
          break;
        default:
          break;
        }
    }

  return dataBytesInObject;
}

int8_t
DRV_CANFDSPI_FifoIndexGet (CANFDSPI_MODULE_ID index, CAN_FIFO_CHANNEL channel,
                           uint8_t *mi)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;

  // Read Status register
  uint8_t b = 0;
  a = cREGADDR_CiFIFOSTA + (channel * CiFIFO_OFFSET);
  a += 1; // byte[1]

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &b);
  if (spiTransferError)
    {
      return -1;
    }

  // Update data
  *mi = b & 0x1f;

  return spiTransferError;
}

CAN_DLC
DRV_CANFDSPI_DataBytesToDlc (uint8_t n)
{
  CAN_DLC dlc = CAN_DLC_0;

  if (n <= 4)
    {
      dlc = CAN_DLC_4;
    }
  else if (n <= 8)
    {
      dlc = CAN_DLC_8;
    }
  else if (n <= 12)
    {
      dlc = CAN_DLC_12;
    }
  else if (n <= 16)
    {
      dlc = CAN_DLC_16;
    }
  else if (n <= 20)
    {
      dlc = CAN_DLC_20;
    }
  else if (n <= 24)
    {
      dlc = CAN_DLC_24;
    }
  else if (n <= 32)
    {
      dlc = CAN_DLC_32;
    }
  else if (n <= 48)
    {
      dlc = CAN_DLC_48;
    }
  else if (n <= 64)
    {
      dlc = CAN_DLC_64;
    }

  return dlc;
}
uint16_t
DRV_CANFDSPI_CalculateCRC16 (uint8_t *data_0, uint16_t size)
{
  uint16_t init = CRCBASE;
  uint8_t index;

  while (size-- != 0)
    {
      index = (((uint8_t) (init >> 8)) & 0xFF) ^ *data_0++;
      //init = (init << 8) ^ crc16_table[index];
    }

  return init;
}
