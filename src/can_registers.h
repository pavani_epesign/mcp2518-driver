/*
 * registers.h
 *
 *  Created on: Jan 16, 2024
 *      Author: vgnsi
 */

#ifndef REGISTERS_H_
#define REGISTERS_H_

#include "stdint.h"
#include "can_defines.h"

// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility
extern "C" {
#endif
// DOM-IGNORE-END

// *****************************************************************************
// *****************************************************************************
/* SPI Instruction Set */

#define cINSTRUCTION_RESET      0x00
#define cINSTRUCTION_READ     0x03
#define cINSTRUCTION_READ_CRC       0x0B
#define cINSTRUCTION_WRITE      0x02
#define cINSTRUCTION_WRITE_CRC      0x0A
#define cINSTRUCTION_WRITE_SAFE     0x0C

// *****************************************************************************
// *****************************************************************************
/* Register Addresses */

/* CAN FD Controller */
#define cREGADDR_CiCON    0x000
#define cREGADDR_CiNBTCFG 0x004
#define cREGADDR_CiDBTCFG 0x008
#define cREGADDR_CiTDC    0x00C

#define cREGADDR_CiTBC      0x010
#define cREGADDR_CiTSCON    0x014
#define cREGADDR_CiVEC      0x018
#define cREGADDR_CiINT      0x01C
#define cREGADDR_CiINTFLAG      cREGADDR_CiINT
#define cREGADDR_CiINTENABLE    (cREGADDR_CiINT+2)

#define cREGADDR_CiRXIF     0x020
#define cREGADDR_CiTXIF     0x024
#define cREGADDR_CiRXOVIF   0x028
#define cREGADDR_CiTXATIF   0x02C

#define cREGADDR_CiTXREQ    0x030
#define cREGADDR_CiTREC     0x034
#define cREGADDR_CiBDIAG0   0x038
#define cREGADDR_CiBDIAG1   0x03C

#define cREGADDR_CiTEFCON   0x040
#define cREGADDR_CiTEFSTA   0x044
#define cREGADDR_CiTEFUA    0x048
#define cREGADDR_CiFIFOBA   0x04C

#define cREGADDR_CiFIFOCON  0x050
#define cREGADDR_CiFIFOSTA  0x054
#define cREGADDR_CiFIFOUA   0x058
#define CiFIFO_OFFSET       (3*4)

#define cREGADDR_CiTXQCON  0x050
#define cREGADDR_CiTXQSTA  0x054
#define cREGADDR_CiTXQUA   0x058

// The filters start right after the FIFO control/status registers
#define cREGADDR_CiFLTCON   (cREGADDR_CiFIFOCON+(CiFIFO_OFFSET*CAN_FIFO_TOTAL_CHANNELS))
#define cREGADDR_CiFLTOBJ   (cREGADDR_CiFLTCON+CAN_FIFO_TOTAL_CHANNELS)
#define cREGADDR_CiMASK     (cREGADDR_CiFLTOBJ+4)

#define CiFILTER_OFFSET     (2*4)

/* MCP25xxFD Specific */
#define cREGADDR_OSC        0xE00
#define cREGADDR_IOCON      0xE04

#define cREGADDR_CRC      0xE08
#define cREGADDR_ECCCON   0xE0C
#define cREGADDR_ECCSTA   0xE10
#ifndef MCP2517FD
#define cREGADDR_DEVID    0xE14
#endif

/* RAM addresses */
//#if defined(MCP2517FD) || defined(MCP2518FD)
#define cRAM_SIZE       2048
//#endif

#define cRAMADDR_START  0x400
#define cRAMADDR_END    (cRAMADDR_START+cRAM_SIZE)

typedef union _REG_t
{
  uint8_t byte[4];
  uint32_t word;
} REG_t;

typedef union _REG_CiCON
{
  struct
  {
    uint16_t DNetFilterCount :5;
    uint16_t IsoCrcEnable :1;
    uint16_t ProtocolExceptionEventDisable :1;
    uint16_t unimplemented1 :1;
    uint16_t WakeUpFilterEnable :1;
    uint16_t WakeUpFilterTime :2;
    uint16_t unimplemented2 :1;
    uint16_t BitRateSwitchDisable :1;
    uint16_t unimplemented3 :3;
  } bF1;
  struct
  {
    uint16_t RestrictReTxAttempts :1;
    uint16_t EsiInGatewayMode :1;
    uint16_t SystemErrorToListenOnly :1;
    uint16_t StoreInTEF :1;
    uint16_t TXQEnable :1;
    uint16_t OpMode :3;
    uint16_t RequestOpMode :3;
    uint16_t AbortAllTx :1;
    uint16_t TxBandWidthSharing :4;
  } bF2;

  uint16_t word;
} REG_CiCON;

// *****************************************************************************
//! FIFO Control Register

typedef union _REG_CiFIFOCON
{
  // Receive FIFO

  struct
  {
    uint16_t RxNotEmptyIE :1;
    uint16_t RxHalfFullIE :1;
    uint16_t RxFullIE :1;
    uint16_t RxOverFlowIE :1;
    uint16_t unimplemented1 :1;
    uint16_t RxTimeStampEnable :1;
    uint16_t unimplemented2 :1;
    uint16_t TxEnable :1;
    uint16_t UINC :1;
    uint16_t unimplemented3 :1;
    uint16_t FRESET :1;
    uint16_t unimplemented4 :5;
  } rxBF1;
  struct
  {
    uint16_t unimplemented4 :8;
    uint16_t FifoSize :5;
    uint16_t PayLoadSize :3;
  } rxBF2;

  // Transmit FIFO

  struct
  {
    uint16_t TxNotFullIE :1;
    uint16_t TxHalfFullIE :1;
    uint16_t TxEmptyIE :1;
    uint16_t unimplemented1 :1;
    uint16_t TxAttemptIE :1;
    uint16_t unimplemented2 :1;
    uint16_t RTREnable :1;
    uint16_t TxEnable :1;
    uint16_t UINC :1;
    uint16_t TxRequest :1;
    uint16_t FRESET :1;
    uint16_t unimplemented3 :5;

  } txBF1;
  struct
  {
    uint16_t TxPriority :5;
    uint16_t TxAttempts :2;
    uint16_t unimplemented4 :1;
    uint16_t FifoSize :5;
    uint16_t PayLoadSize :3;
  } txBF2;

  uint16_t word;
  uint8_t byte[2];
} REG_CiFIFOCON;

typedef union _REG_CiNBTCFG
{

  struct
  {
    uint16_t SJW :4;
    uint16_t unimplemented1 :4;
    uint16_t TSEG2 :4;
    uint16_t unimplemented2 :4;

  } bF1;
  struct
  {
    uint16_t TSEG1 :5;
    uint16_t unimplemented3 :3;
    uint16_t BRP :8;
  } bF2;
  uint16_t word;
//uint8_t byte[4];
} REG_CiNBTCFG;

typedef union _REG_CiDBTCFG
{
  struct
  {
    uint16_t SJW :4;
    uint16_t unimplemented1 :4;
    uint16_t TSEG2 :4;
    uint16_t unimplemented2 :4;
  } bF1;
  struct
  {
    uint16_t TSEG1 :5;
    uint16_t unimplemented3 :3;
    uint16_t BRP :8;
  } bF2;
  uint16_t word;
} REG_CiDBTCFG;

typedef union _REG_CiTDC
{
  struct
  {
    uint16_t TDCValue :6;
    uint16_t unimplemented1 :2;
    uint16_t TDCOffset :7;
    uint16_t unimplemented2 :1;
  } bF1;
  struct
  {
    uint16_t TDCMode :2;
    uint16_t unimplemented3 :6;
    uint16_t SID11Enable :1;
    uint16_t EdgeFilterEnable :1;
    uint16_t unimplemented4 :6;
  } bF2;
  uint16_t word;
} REG_CiTDC;

typedef union _REG_CiTXQCON
{
  // Transmit FIFO

  struct
  {
    uint16_t TxNotFullIE :1;
    uint16_t TxHalfFullIE :1;
    uint16_t TxEmptyIE :1;
    uint16_t unimplemented1 :1;
    uint16_t TxAttemptIE :1;
    uint16_t unimplemented2 :1;
    uint16_t RTREnable :1;
    uint16_t TxEnable :1;
    uint16_t UINC :1;
    uint16_t TxRequest :1;
    uint16_t FRESET :1;
    uint16_t unimplemented3 :5;

  } txBF1;
  struct
  {
    uint16_t TxPriority :5;
    uint16_t TxAttempts :2;
    uint16_t unimplemented4 :1;
    uint16_t FifoSize :5;
    uint16_t PayLoadSize :3;
  } txBF2;

  uint16_t word;
  uint8_t byte[2];
} REG_CiTXQCON;
typedef union _REG_CiFLTOBJ
{
  CAN_FILTEROBJ_ID bF;
  uint32_t word;
  uint8_t byte[4];
} REG_CiFLTOBJ;

// *****************************************************************************
//! Mask Object Register

typedef union _REG_CiMASK
{
  CAN_MASKOBJ_ID bF;
  uint32_t word;
  uint8_t byte[4];
} REG_CiMASK;

typedef union _REG_CiFIFOSTA1
{
  uint32_t word;
  uint8_t byte[4];
} REG_CiFIFOSTA1;

typedef union _REG_CiFIFOCON1
{
  uint32_t word;
  uint8_t byte[4];
} REG_CiFIFOCON1;

typedef union _REG_CiFIFOUA1
{

  uint32_t word;
  uint8_t byte[4];
} REG_CiFIFOUA1;
typedef union _REG_CiFLTCON_BYTE
{
  struct
  {
    uint8_t BufferPointer :5;
    uint8_t unimplemented1 :2;
    uint8_t Enable :1;
  } bF;
  uint8_t byte;
} REG_CiFLTCON_BYTE;

typedef union _REG_IOCON
{

  struct
  {
    uint16_t TRIS0 :1;
    uint16_t TRIS1 :1;
    uint16_t unimplemented1 :2;
    uint16_t ClearAutoSleepOnMatch :1;
    uint16_t AutoSleepEnable :1;
    uint16_t XcrSTBYEnable :1;
    uint16_t unimplemented2 :1;
    uint16_t LAT0 :1;
    uint16_t LAT1 :1;
    uint16_t unimplemented3 :5;
    uint16_t HVDETSEL :1;
  } bF1;
  struct
  {
    uint16_t GPIO0 :1;
    uint16_t GPIO1 :1;
    uint16_t unimplemented4 :6;
    uint16_t PinMode0 :1;
    uint16_t PinMode1 :1;
    uint16_t unimplemented5 :2;
    uint16_t TXCANOpenDrain :1;
    uint16_t SOFOutputEnable :1;
    uint16_t INTPinOpenDrain :1;
    uint16_t unimplemented6 :1;
  } bF2;
  uint16_t word;
  uint8_t byte[2];
} REG_IOCON;

typedef struct _CAN_INT_ENABLES
{

  uint16_t TXIE :1;
  uint16_t RXIE :1;
  uint16_t TBCIE :1;
  uint16_t MODIE :1;
  uint16_t TEFIE :1;
  uint16_t unimplemented2 :3;
  uint16_t ECCIE :1;
  uint16_t SPICRCIE :1;
  uint16_t TXATIE :1;
  uint16_t RXOVIE :1;
  uint16_t SERRIE :1;
  uint16_t CERRIE :1;
  uint16_t WAKIE :1;
  uint16_t IVMIE :1;

} CAN_INT_ENABLES;

typedef union _REG_CiINTENABLE
{
  CAN_INT_ENABLES IE;
  uint16_t word;
  uint8_t byte[2];
} REG_CiINTENABLE;

typedef union _REG_CiINTFLAG
{
  CAN_INT_FLAGS IF;
  uint16_t word;
  uint8_t byte[2];
} REG_CiINTFLAG;

typedef union _REG_CiTREC
{

  uint32_t word;
  uint8_t byte[4];
} REG_CiTREC;

typedef union _REG_ECCSTA
{

  struct
  {
    uint16_t unimplemented1 :1;
    uint16_t SECIF :1;
    uint16_t DEDIF :1;
    uint16_t unimplemented2 :13;
  } bF1;
  struct
  {

    uint16_t ErrorAddress :12;
    uint16_t unimplemented3 :4;
  } bF2;
  uint16_t word;
  uint8_t byte[2];
} REG_ECCSTA;

typedef union _REG_OSC
{

  struct
  {
    uint16_t PllEnable :1;
    uint16_t unimplemented1 :1;
    uint16_t OscDisable :1;
#ifdef MCP2517FD
        uint16_t unimplemented2 : 1;
#else
    uint16_t LowPowerModeEnable :1;
#endif
    uint16_t SCLKDIV :1;
    uint16_t CLKODIV :2;
    uint16_t unimplemented3 :1;
    uint16_t PllReady :1;
    uint16_t unimplemented4 :1;
    uint16_t OscReady :1;
    uint16_t unimplemented5 :1;
    uint16_t SclkReady :1;
    uint16_t unimplemented6 :3;
  } bF1;
  struct
  {
    uint16_t unimplemented6 :16;
  } bF2;
  uint16_t word;
  uint8_t byte[2];
} REG_OSC;

typedef union _REG_CiTEFSTA
{

  struct
  {
    uint16_t TEFNotEmptyIF :1;
    uint16_t TEFHalfFullIF :1;
    uint16_t TEFFullIF :1;
    uint16_t TEFOVIF :1;
    uint16_t unimplemented1 :12;
  } bF1;
  struct
  {
    uint16_t unimplemented1 :16;
  } bF2;
  uint16_t word;
  uint8_t byte[2];
} REG_CiTEFSTA;

typedef union _REG_CiTEFCON
{

  struct
  {
    uint16_t TEFNEIE :1;
    uint16_t TEFHFIE :1;
    uint16_t TEFFULIE :1;
    uint16_t TEFOVIE :1;
    uint16_t unimplemented1 :1;
    uint16_t TimeStampEnable :1;
    uint16_t unimplemented2 :2;
    uint16_t UINC :1;
    uint16_t unimplemented3 :1;
    uint16_t FRESET :1;
    uint16_t unimplemented4 :5;
  } bF1;
  struct
  {
    uint16_t unimplemented4 :8;
    uint16_t FifoSize :5;
    uint16_t unimplemented5 :3;
  } bF2;
  uint32_t word;
  uint8_t byte[4];
} REG_CiTEFCON;
#endif /* REGISTERS_H_ */
