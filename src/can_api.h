/*
 * api.h
 *
 *  Created on: Jan 16, 2024
 *      Author: vgnsi
 */

#ifndef API_H_
#define API_H_

// *****************************************************************************
// *****************************************************************************
// Section: Included Files

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>

#include "can_defines.h"
#include "can_registers.h"

// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility
extern "C" {
#endif
// DOM-IGNORE-END

typedef uint8_t CANFDSPI_MODULE_ID;

// *****************************************************************************
// *****************************************************************************
//! Reset DUT

int8_t
DRV_CANFDSPI_Reset (CANFDSPI_MODULE_ID index);

// *****************************************************************************
// *****************************************************************************
// Section: SPI Access Functions

// *****************************************************************************
//! SPI Read Byte

int8_t
DRV_CANFDSPI_ReadByte (CANFDSPI_MODULE_ID index, uint16_t address, uint8_t *rxd);

// *****************************************************************************
//! SPI Write Byte

int8_t
DRV_CANFDSPI_WriteByte (CANFDSPI_MODULE_ID index, uint16_t address, uint8_t txd);

// *****************************************************************************
//! SPI Read Word

int8_t
DRV_CANFDSPI_ReadWord (CANFDSPI_MODULE_ID index, uint16_t address,
                       uint32_t *rxd);

// *****************************************************************************
//! SPI Write Word

int8_t
DRV_CANFDSPI_WriteWord (CANFDSPI_MODULE_ID index, uint16_t address,
                        uint32_t txd);

/// *****************************************************************************
//! SPI Read Word

int8_t
DRV_CANFDSPI_ReadHalfWord (CANFDSPI_MODULE_ID index, uint16_t address,
                           uint16_t *rxd);

// *****************************************************************************
//! SPI Write Word

int8_t
DRV_CANFDSPI_WriteHalfWord (CANFDSPI_MODULE_ID index, uint16_t address,
                            uint16_t txd);

// *****************************************************************************
//! SPI Read Byte Array

int8_t
DRV_CANFDSPI_ReadByteArray (CANFDSPI_MODULE_ID index, uint16_t address,
                            uint8_t *rxd, uint16_t nBytes);

// *****************************************************************************
//! SPI Write Byte Array

int8_t
DRV_CANFDSPI_WriteByteArray (CANFDSPI_MODULE_ID index, uint16_t address,
                             uint8_t *txd, uint16_t nBytes);

// *****************************************************************************
//! SPI SFR Write Byte Safe
/*!
 * Writes Byte to SFR at address using SPI CRC. Byte gets only written if CRC matches.
 *
 * Remark: The function doesn't check if the address is an SFR address.
 */

int8_t
DRV_CANFDSPI_WriteByteSafe (CANFDSPI_MODULE_ID index, uint16_t address,
                            uint8_t txd);

// *****************************************************************************
//! SPI RAM Write Word Safe
/*!
 * Writes Word to RAM at address using SPI CRC. Word gets only written if CRC matches.
 *
 * Remark: The function doesn't check if the address is a RAM address.
 */

int8_t
DRV_CANFDSPI_WriteWordSafe (CANFDSPI_MODULE_ID index, uint16_t address,
                            uint32_t txd);

// *****************************************************************************
//! SPI Read Byte Array with CRC

int8_t
DRV_CANFDSPI_ReadByteArrayWithCRC (CANFDSPI_MODULE_ID index, uint16_t address,
                                   uint8_t *rxd, uint16_t nBytes, bool fromRam,
                                   uint8_t *crcIsCorrect);

// *****************************************************************************
//! SPI Write Byte Array with CRC

int8_t
DRV_CANFDSPI_WriteByteArrayWithCRC (CANFDSPI_MODULE_ID index, uint16_t address,
                                    uint8_t *txd, uint16_t nBytes, bool fromRam);

// *****************************************************************************
//! SPI Read Word Array

int8_t
DRV_CANFDSPI_ReadWordArray (CANFDSPI_MODULE_ID index, uint16_t address,
                            uint32_t *rxd, uint16_t nWords);

// *****************************************************************************
//! SPI Write Word Array

int8_t
DRV_CANFDSPI_WriteWordArray (CANFDSPI_MODULE_ID index, uint16_t address,
                             uint32_t *txd, uint16_t nWords);

int8_t
DRV_CANFDSPI_EccEnable (CANFDSPI_MODULE_ID index);
int8_t
DRV_CANFDSPI_Reset (CANFDSPI_MODULE_ID index);
int8_t
DRV_CANFDSPI_RamInit (CANFDSPI_MODULE_ID index, uint8_t d);
int8_t
DRV_CANFDSPI_Configure (CANFDSPI_MODULE_ID index, CAN_CONFIG *config);
int8_t
DRV_CANFDSPI_ReceiveChannelConfigure (CANFDSPI_MODULE_ID index,
                                      CAN_FIFO_CHANNEL channel,
                                      CAN_RX_FIFO_CONFIG *config);
int8_t
DRV_CANFDSPI_ReceiveChannelUpdate (CANFDSPI_MODULE_ID index,
                                   CAN_FIFO_CHANNEL channel);
int8_t
DRV_CANFDSPI_ReceiveMessageGet (CANFDSPI_MODULE_ID index,
                                CAN_FIFO_CHANNEL channel, CAN_RX_MSGOBJ *rxObj,
                                uint8_t *rxd, uint8_t nBytes);
int8_t
DRV_CANFDSPI_NominalBitTimeConfigure (CANFDSPI_MODULE_ID index,
                               CAN_BITTIME_SETUP bitTime, CAN_SYSCLK_SPEED clk);
int8_t
DRV_CANFDSPI_DataBitTimeConfigure (CANFDSPI_MODULE_ID index,
                               CAN_BITTIME_SETUP bitTime, CAN_SYSCLK_SPEED clk);
int8_t
DRV_CANFDSPI_BitTimeConfigureNominal40MHz (CANFDSPI_MODULE_ID index,
                                           CAN_BITTIME_SETUP bitTime);
int8_t
DRV_CANFDSPI_BitTimeConfigureData40MHz (CANFDSPI_MODULE_ID index,
                                        CAN_BITTIME_SETUP bitTime);
int8_t
DRV_CANFDSPI_BitTimeConfigureNominal20MHz (CANFDSPI_MODULE_ID index,
                                           CAN_BITTIME_SETUP bitTime);
int8_t
DRV_CANFDSPI_BitTimeConfigureData20MHz (CANFDSPI_MODULE_ID index,
                                        CAN_BITTIME_SETUP bitTime);
int8_t
DRV_CANFDSPI_BitTimeConfigureNominal10MHz (CANFDSPI_MODULE_ID index,
                                           CAN_BITTIME_SETUP bitTime);
int8_t
DRV_CANFDSPI_BitTimeConfigureData10MHz (CANFDSPI_MODULE_ID index,
                                        CAN_BITTIME_SETUP bitTime);
int8_t
DRV_CANFDSPI_OperationModeSelect (CANFDSPI_MODULE_ID index,
                                  CAN_OPERATION_MODE opMode);
int8_t
DRV_CANFDSPI_ReceiveChannelEventGet (CANFDSPI_MODULE_ID index,
                                     CAN_FIFO_CHANNEL channel,
                                     CAN_RX_FIFO_EVENT *flags);
int8_t
DRV_CANFDSPI_FilterObjectConfigure (CANFDSPI_MODULE_ID index, CAN_FILTER filter,
                                    CAN_FILTEROBJ_ID *id);
int8_t
DRV_CANFDSPI_FilterMaskConfigure (CANFDSPI_MODULE_ID index, CAN_FILTER filter,
                                  CAN_MASKOBJ_ID *mask);
int8_t
DRV_CANFDSPI_FilterToFifoLink (CANFDSPI_MODULE_ID index, CAN_FILTER filter,
                               CAN_FIFO_CHANNEL channel, bool enable);
int8_t
DRV_CANFDSPI_ReceiveChannelEventEnable (CANFDSPI_MODULE_ID index,
                                        CAN_FIFO_CHANNEL channel,
                                        CAN_RX_FIFO_EVENT flags);

int8_t
DRV_CANFDSPI_GpioModeConfigure (CANFDSPI_MODULE_ID index, GPIO_PIN_MODE gpio0,
                                GPIO_PIN_MODE gpio1);
int8_t
DRV_CANFDSPI_ModuleEventEnable (CANFDSPI_MODULE_ID index,
                                CAN_MODULE_EVENT flags);
int8_t
DRV_CANFDSPI_ModuleEventDisable (CANFDSPI_MODULE_ID index,
                                 CAN_MODULE_EVENT flags);
int8_t
DRV_CANFDSPI_ModuleEventClear (CANFDSPI_MODULE_ID index, CAN_MODULE_EVENT flags);
int8_t
DRV_CANFDSPI_ModuleEventRxCodeGet (CANFDSPI_MODULE_ID index, CAN_RXCODE *rxCode);
int8_t
DRV_CANFDSPI_ModuleEventTxCodeGet (CANFDSPI_MODULE_ID index, CAN_TXCODE *txCode);
int8_t
DRV_CANFDSPI_ModuleEventFilterHitGet (CANFDSPI_MODULE_ID index,
                                      CAN_FILTER *filterHit);
int8_t
DRV_CANFDSPI_ModuleEventIcodeGet (CANFDSPI_MODULE_ID index, CAN_ICODE *icode);

int8_t
DRV_CANFDSPI_TransmitChannelConfigure (CANFDSPI_MODULE_ID index,
                                       CAN_FIFO_CHANNEL channel,
                                       CAN_TX_FIFO_CONFIG *config);
int8_t
DRV_CANFDSPI_TransmitChannelUpdate (CANFDSPI_MODULE_ID index,
                                    CAN_FIFO_CHANNEL channel, bool flush);
int8_t
DRV_CANFDSPI_TransmitChannelFlush (CANFDSPI_MODULE_ID index,
                                   CAN_FIFO_CHANNEL channel);
int8_t
DRV_CANFDSPI_FilterDisable (CANFDSPI_MODULE_ID index, CAN_FILTER filter);
int8_t
DRV_CANFDSPI_ReceiveChannelUpdate (CANFDSPI_MODULE_ID index,
                                   CAN_FIFO_CHANNEL channel);
int8_t
DRV_CANFDSPI_ReceiveChannelReset (CANFDSPI_MODULE_ID index,
                                  CAN_FIFO_CHANNEL channel);
int8_t
DRV_CANFDSPI_ReceiveEventOverflowGet (CANFDSPI_MODULE_ID index,
                                      uint32_t *rxovif);
int8_t
DRV_CANFDSPI_ReceiveEventGet (CANFDSPI_MODULE_ID index, uint32_t *rxif);

// *****************************************************************************
//! Time Stamp Enable

int8_t
DRV_CANFDSPI_TimeStampEnable (CANFDSPI_MODULE_ID index);

// *****************************************************************************
//! Time Stamp Disable

int8_t
DRV_CANFDSPI_TimeStampDisable (CANFDSPI_MODULE_ID index);

// *****************************************************************************
//! Time Stamp Get

int8_t
DRV_CANFDSPI_TimeStampGet (CANFDSPI_MODULE_ID index, uint32_t *ts);

// *****************************************************************************
//! Time Stamp Set

int8_t
DRV_CANFDSPI_TimeStampSet (CANFDSPI_MODULE_ID index, uint32_t ts);

// *****************************************************************************
//! Time Stamp Mode Configure

int8_t
DRV_CANFDSPI_TimeStampModeConfigure (CANFDSPI_MODULE_ID index, CAN_TS_MODE mode);

// *****************************************************************************
//! Time Stamp Prescaler Set

int8_t
DRV_CANFDSPI_TimeStampPrescalerSet (CANFDSPI_MODULE_ID index, uint16_t ps);

int8_t
DRV_CANFDSPI_OscillatorControlSet (CANFDSPI_MODULE_ID index, CAN_OSC_CTRL ctrl);

int8_t
DRV_CANFDSPI_OscillatorControlObjectReset (CAN_OSC_CTRL *ctrl);

// *****************************************************************************
//! Get Oscillator Status

int8_t
DRV_CANFDSPI_OscillatorStatusGet (CANFDSPI_MODULE_ID index,
                                  CAN_OSC_STATUS *status);

// *****************************************************************************
//! DLC to number of actual data bytes conversion

uint32_t
DRV_CANFDSPI_DlcToDataBytes (CAN_DLC dlc);

// *****************************************************************************
//! FIFO Index Get

int8_t
DRV_CANFDSPI_FifoIndexGet (CANFDSPI_MODULE_ID index, CAN_FIFO_CHANNEL channel,
                           uint8_t *mi);

// *****************************************************************************
//! Calculate CRC16

uint16_t
DRV_CANFDSPI_CalculateCRC16 (uint8_t *data_0, uint16_t size);

// *****************************************************************************
//! Data bytes to DLC conversion

CAN_DLC
DRV_CANFDSPI_DataBytesToDlc (uint8_t n);

//! Enable Transceiver Standby Control

int8_t
DRV_CANFDSPI_GpioStandbyControlEnable (CANFDSPI_MODULE_ID index);

// *****************************************************************************
//! Disable Transceiver Standby Control

int8_t
DRV_CANFDSPI_GpioStandbyControlDisable (CANFDSPI_MODULE_ID index);

// *****************************************************************************
//! Configure Open Drain Interrupts

int8_t
DRV_CANFDSPI_GpioInterruptPinsOpenDrainConfigure (CANFDSPI_MODULE_ID index,
                                                  GPIO_OPEN_DRAIN_MODE mode);

// *****************************************************************************
//! Configure Open Drain TXCAN

int8_t
DRV_CANFDSPI_GpioTransmitPinOpenDrainConfigure (CANFDSPI_MODULE_ID index,
                                                GPIO_OPEN_DRAIN_MODE mode);

// *****************************************************************************
//! GPIO Output Pin Set

int8_t
DRV_CANFDSPI_GpioPinSet (CANFDSPI_MODULE_ID index, GPIO_PIN_POS pos,
                         GPIO_PIN_STATE latch);

// *****************************************************************************
//! GPIO Input Pin Read

int8_t
DRV_CANFDSPI_GpioPinRead (CANFDSPI_MODULE_ID index, GPIO_PIN_POS pos,
                          GPIO_PIN_STATE *state);

// *****************************************************************************
//! Configure CLKO Pin

int8_t
DRV_CANFDSPI_GpioClockOutputConfigure (CANFDSPI_MODULE_ID index,
                                       GPIO_CLKO_MODE mode);

int8_t
DRV_CANFDSPI_GpioModeConfigure (CANFDSPI_MODULE_ID index, GPIO_PIN_MODE gpio0,
                                GPIO_PIN_MODE gpio1);

// *****************************************************************************
//! Initialize GPIO Direction

int8_t
DRV_CANFDSPI_GpioDirectionConfigure (CANFDSPI_MODULE_ID index,
                                     GPIO_PIN_DIRECTION gpio0,
                                     GPIO_PIN_DIRECTION gpio1);
int8_t
DRV_CANFDSPI_TefEventGet (CANFDSPI_MODULE_ID index, CAN_TEF_FIFO_EVENT *flags);

// *****************************************************************************
//! Transmit Event FIFO Event Enable
/*!
 * Enables Transmit Event FIFO interrupts
 */

int8_t
DRV_CANFDSPI_TefEventEnable (CANFDSPI_MODULE_ID index, CAN_TEF_FIFO_EVENT flags);

// *****************************************************************************
//! Transmit Event FIFO Event Disable
/*!
 * Disables Transmit Event FIFO interrupts
 */

int8_t
DRV_CANFDSPI_TefEventDisable (CANFDSPI_MODULE_ID index,
                              CAN_TEF_FIFO_EVENT flags);

// *****************************************************************************
//! Transmit Event FIFO Event Clear
/*!
 * Clears Transmit Event FIFO Overflow interrupt Flag
 */

int8_t
DRV_CANFDSPI_TefEventOverflowClear (CANFDSPI_MODULE_ID index);

#endif /* API_H_ */
